# slack-clone

## Author

👤 **ANDRIANANTENAINA Herilova Jenny Rinah**

## Slack clone [React, Apollo-client, Apollo-server, postgres , nexus and prisma ].

## Preview

<img src="preview0.png" />
<img src="preview1.png" />
<img src="preview2.png" />

## Launch project

## Server

Config

```sh
copy server/graphql/.env.example file to server/graphql/.env.local
copy server/graphql/.env.example file to server/graphql/.env
```

Start docker

```sh
cd server && docker-compose up -d
```

Start server

```sh
docker-compose logs -f graphql
```

## Client

Config

```sh
copy client/web/.env.example file to client/web/.env
```

Install package

```sh
cd client/web && yarn install
```

Start client

```sh
yarn start
```

## Default seed user

```sh
-  kevin@gmail.com (kevin@1234)
-  julien@gmail.com (julien@1234)
```
