import { Avatar as MaterialAvatar, Tooltip } from '@mui/material';
import classnames from 'classnames';
import { FC, HTMLAttributes } from 'react';
import { OnlineIndicator } from '../OnlineIndicator';
import useStyle from './styles';

interface AvatarProps {
  userName: string;
  indicatorStatus?: 'isMine' | 'offline' | 'online';
  size?: number;
  isCircle?: boolean;
}

const Avatar: FC<AvatarProps & HTMLAttributes<HTMLDivElement>> = props => {
  const { isCircle, userName, indicatorStatus, className, size, ...otherProps } = props;

  const classes = useStyle();
  return (
    <div
      style={{
        maxHeight: size,
        maxWidth: size,
      }}
      {...otherProps}
      className={classnames(classes.containerAvatar, className)}
    >
      <Tooltip title={userName} enterDelay={200}>
        <MaterialAvatar
          style={{
            width: size,
            height: size,
          }}
          variant={!isCircle ? 'rounded' : 'circular'}
        >
          {userName.substr(0, 1).toUpperCase()}
        </MaterialAvatar>
      </Tooltip>
      {indicatorStatus && (
        <OnlineIndicator size={8} status={indicatorStatus} className={classes.onlineIndicator} />
      )}
    </div>
  );
};

Avatar.defaultProps = {
  indicatorStatus: undefined,
  size: undefined,
  isCircle: false,
};

export default Avatar;
