import { makeStyles } from '@mui/styles';

export default makeStyles(() => ({
  containerAvatar: {
    position: 'relative',
    display: 'inline-block',
  },
  onlineIndicator: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    width: 2,
    height: 2,
  },
}));
