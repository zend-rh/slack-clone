import { Typography } from '@mui/material';
import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { CustomListItem } from '../CustomListItem';
import useStyle from './styles';

interface ChannelMenuItemsProps {
  name: string;
  spaceId: string;
  id: string;
  isActive: boolean;
  isPrivate: boolean;
}

const ChannelMenuItem: FC<ChannelMenuItemsProps> = props => {
  const { name, id, isActive, isPrivate, spaceId } = props;
  const history = useHistory();
  const classes = useStyle();

  const onClick = () => history.push(`/space/${spaceId}/channel/${id}`);

  return (
    <CustomListItem
      selected={isActive}
      className={classes.containerChannelMenuItem}
      onClick={onClick}
    >
      <Typography
        paddingLeft={2}
        color={!isPrivate && !isActive ? '#D1BFCF' : '#D1BFCF'}
        variant="body2"
      >
        <span>#</span>
        <span className={classes.name}>{name}</span>
      </Typography>
    </CustomListItem>
  );
};

export default ChannelMenuItem;
