import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerChannelMenuItem: {
    width: '100%',
    paddingLeft: 60,
    height: 30,
  },
  name: {
    marginLeft: 15,
  },
}));
