import { Button as MaterialButton, ButtonProps as DefaultButtonProps } from '@mui/material';
import classnames from 'classnames';
import { FC } from 'react';
import { Loading } from '../Loading';
import useStyle from './styles';

interface ButtonProps {
  isLoading?: boolean;
}

const CustomBUtton: FC<ButtonProps & DefaultButtonProps> = props => {
  const { isLoading, color, variant, classes: classesProps, children, ...defaultProps } = props;
  const classes = useStyle();
  return (
    <MaterialButton
      color={color || 'primary'}
      variant={variant || 'contained'}
      {...defaultProps}
      classes={{
        ...classesProps,
        text: classnames(classes.text, classesProps?.text),
      }}
    >
      {children}
      {isLoading && (
        <Loading
          circularProgressProps={{
            color: 'secondary',
          }}
          className={classes.loading}
          size={12}
        />
      )}
    </MaterialButton>
  );
};

CustomBUtton.defaultProps = {
  isLoading: false,
};

export default CustomBUtton;
