import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerCustomButton: {},
  text: {
    fontWeight: 600,
  },
  loading: {
    marginLeft: 7,
  },
}));
