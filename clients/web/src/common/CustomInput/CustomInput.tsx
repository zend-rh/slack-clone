import { Send } from '@mui/icons-material';
import { IconButton, TextField, TextFieldProps, Tooltip } from '@mui/material';
import classnames from 'classnames';
import { FC } from 'react';
import useStyle from './styles';

interface CustomInputProps {
  value: string;
  setValue: (value: string) => void;
  onSubmit: () => void;
}

const CustomInput: FC<CustomInputProps & TextFieldProps> = props => {
  const { value, setValue, className, onSubmit, ...otherProps } = props;
  const classes = useStyle();
  return (
    <div className={classes.containerInput}>
      <TextField
        {...otherProps}
        value={value}
        className={classnames(classes.containerCustomInput, className)}
        onChange={evt => setValue(evt.target.value)}
      />
      <Tooltip title="Send message">
        <IconButton onClick={onSubmit} className={classes.sendButton}>
          <Send />
        </IconButton>
      </Tooltip>
    </div>
  );
};

export default CustomInput;
