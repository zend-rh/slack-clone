import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerInput: {
    position: 'relative',
    flex: 1,
    display: 'flex',
  },
  containerCustomInput: {
    paddingLeft: 25,
    paddingRight: 45,
  },
  sendButton: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
}));
