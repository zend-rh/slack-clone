import { ListItem, ListItemProps, ListItemText, Typography } from '@mui/material';
import classnames from 'classnames';
import { FC } from 'react';
import useStyle from './styles';

interface CustomListItemProps {
  onClick: () => void;
}

const CustomListItem: FC<ListItemProps<any> & CustomListItemProps> = props => {
  const classes = useStyle();
  const { selected, children, className } = props;
  return (
    <ListItem
      {...props}
      button={true}
      className={classnames(
        classes.containerCustomListItem,
        selected && classes.selected,
        className
      )}
    >
      <ListItemText>
        <Typography
          className={classnames(selected && classes.textSelected)}
          color="textSecondary"
          variant="body2"
        >
          {children}
        </Typography>
      </ListItemText>
    </ListItem>
  );
};

export default CustomListItem;
