import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { important } from '../../utils/materialUtils';

export default makeStyles((theme: Theme) => ({
  containerCustomListItem: {},
  selected: {
    backgroundColor: important('#2C4AA7'),
  },
  textSelected: {
    color: '#D1BFCF',
  },
}));
