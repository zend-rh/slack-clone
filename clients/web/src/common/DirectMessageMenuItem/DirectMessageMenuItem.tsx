import { Stack } from '@mui/material';
import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { Avatar } from '../Avatar';
import { CustomListItem } from '../CustomListItem';
import { Indicator } from '../OnlineIndicator/Indicator';
import useStyle from './styles';

interface DirectMessageMenuItemsProps {
  userName: string;
  channelId: string;
  spaceId: string;
  isActive: boolean;
  isOnline: boolean;
}

const DirectMessageMenuItem: FC<DirectMessageMenuItemsProps> = props => {
  const { userName, channelId, isActive, isOnline, spaceId } = props;
  const history = useHistory();
  const classes = useStyle();

  const onClick = () => history.push(`/space/${spaceId}/user/${channelId}`);

  return (
    <CustomListItem
      selected={isActive}
      className={classes.containerDirectMessageMenuItem}
      onClick={onClick}
    >
      <Stack direction="row" alignItems="center">
        <Avatar size={25} userName={userName} />
        <span className={classes.indicator}>
          {userName}
          {!isOnline && <p className={classes.margin} />}
          {isOnline && <Indicator size={10} isOffline={false} />}
        </span>
      </Stack>
    </CustomListItem>
  );
};

export default DirectMessageMenuItem;
