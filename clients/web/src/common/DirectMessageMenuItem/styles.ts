import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerDirectMessageMenuItem: {
    width: '100%',
    paddingLeft: 30,
    height: 35,
  },
  indicator: {
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
    color: '#D1BFCF',
    marginLeft: 10,
  },
  margin: {
    marginLeft: 10,
  },
}));
