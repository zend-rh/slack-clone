import { ApolloError } from '@apollo/client';
import { Typography } from '@mui/material';
import classnames from 'classnames';
import { FC } from 'react';
import { Redirect } from 'react-router-dom';
import authService from '../../services/authServices';
import ErrorMessage from '../../services/ErrorMessage';
import useStyle from './styles';

interface ErrorComponentProps {
  error: ApolloError;
  isFullWidth?: boolean;
}

const ErrorComponent: FC<ErrorComponentProps> = props => {
  const { error, isFullWidth } = props;
  const classes = useStyle();

  let message = 'Oups, an error occured';

  if (error.networkError) {
    message = 'Network error';
  } else if (error.graphQLErrors.length) {
    const firstMessage = error.graphQLErrors[0].message;
    if (firstMessage === ErrorMessage.NOT_AUTHENTICATED) {
      authService.clear();
      return <Redirect to="/login" />;
    }
  }

  return (
    <div className={classnames(classes.containerErrorComponent, isFullWidth && classes.fullWidth)}>
      <Typography color="error" variant="h5">
        {message}
      </Typography>
    </div>
  );
};

ErrorComponent.defaultProps = {
  isFullWidth: false,
};

export default ErrorComponent;
