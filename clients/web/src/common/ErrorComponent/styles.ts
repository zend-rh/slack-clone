import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerErrorComponent: {
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fullWidth: {
    height: '100%',
    width: '100%',
  },
}));
