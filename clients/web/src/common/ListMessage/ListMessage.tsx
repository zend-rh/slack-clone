import { FC, useEffect, useRef } from 'react';
import { MessageInfosFragment } from '../../generated/graphql';
import { MessageItem } from '../MessageItem';
import useStyle from './styles';

interface ListMessageProps {
  messages: MessageInfosFragment[];
}

const ListMessage: FC<ListMessageProps> = props => {
  const { messages } = props;
  const classes = useStyle();
  const divRef = useRef<HTMLDivElement>(null);
  const messageLength = messages.length;
  useEffect(() => {
    setTimeout(() => {
      if (divRef.current) {
        divRef.current.scrollTo({
          top: divRef.current.scrollHeight,
          behavior: 'smooth',
        });
      }
    }, 1);
  }, [messageLength]);

  return (
    <div ref={divRef} className={classes.containerListMessage}>
      {messages.map(m => (
        <div className={classes.messages} key={m.id}>
          <MessageItem message={m} />
        </div>
      ))}
    </div>
  );
};

export default ListMessage;
