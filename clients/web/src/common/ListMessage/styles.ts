import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerListMessage: {
    padding: '14px 21px',
    overflowY: 'auto',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  messages: {
    marginTop: 28,
  },
}));
