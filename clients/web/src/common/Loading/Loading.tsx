import { CircularProgress, CircularProgressProps } from '@mui/material';
import classnames from 'classnames';
import { FC, HTMLAttributes } from 'react';
import useStyles from './styles';

interface LoadingProps {
  isFullHeight?: boolean;
  size?: number;
  circularProgressProps?: CircularProgressProps;
}

const Loading: FC<LoadingProps & HTMLAttributes<HTMLDivElement>> = props => {
  const { className, isFullHeight, size, circularProgressProps, ...otherProps } = props;
  const classes = useStyles();
  return (
    <div
      {...otherProps}
      className={classnames(
        classes.containerLoading,
        isFullHeight && classes.containerLoadingFullHeight,
        className
      )}
    >
      <CircularProgress {...circularProgressProps} size={size || circularProgressProps?.size} />
    </div>
  );
};

Loading.defaultProps = {
  isFullHeight: false,
  size: undefined,
  circularProgressProps: undefined,
};

export default Loading;
