import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerLoading: {
    alignSelf: 'center',
  },
  containerLoadingFullHeight: {
    height: '100%',
    width: '100%',
    ...theme.containerStyles.flexColumn,
    alignItems: 'center',
    justifyContent: 'center',
  },
}));
