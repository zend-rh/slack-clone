/* eslint-disable react/require-default-props */
/* eslint-disable jsx-a11y/alt-text */
import { FC } from 'react';
import logo from '../../assets/slack.png';

interface IProps {
  size?: number;
}
const Logo: FC<IProps> = ({ size }) => {
  return <img src={logo} width={size || 30} height={size || 30} />;
};

export default Logo;
