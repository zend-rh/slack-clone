import { FC } from 'react';
import { MessageInfosFragment } from '../../generated/graphql';
import { getUserName } from '../../utils/userUtils';
import { Avatar } from '../Avatar';
import { TimeAgo } from '../TimeAgo';
import MessageItemSender from './MessageItemSender/MessageItemSender';
import { MessageItemText } from './MessageItemText';
import useStyle from './styles';

interface MessageItemProps {
  message: MessageInfosFragment;
}

const MessageItem: FC<MessageItemProps> = props => {
  const { message } = props;
  const { text, author, createdAt } = message;
  const name = getUserName(author as any);
  const classes = useStyle();
  return (
    <div className={classes.containerMessageItem}>
      <Avatar
        indicatorStatus={author?.isMe ? 'isMine' : author?.isOnline ? 'online' : 'offline'}
        size={40}
        userName={name}
      />
      <div className={classes.messageContainer}>
        <div className={classes.authorContainer}>
          <MessageItemSender userName={name} />
          <TimeAgo className={classes.time} dateTime={new Date(createdAt)} />
        </div>
        <MessageItemText text={text || ''} />
      </div>
    </div>
  );
};

export default MessageItem;
