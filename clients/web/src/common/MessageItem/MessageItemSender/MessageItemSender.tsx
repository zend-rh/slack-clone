import { Typography } from '@mui/material';
import { FC } from 'react';
import useStyle from './styles';

interface MessageItemSenderProps {
  userName: string;
}

const MessageItemSender: FC<MessageItemSenderProps> = props => {
  const { userName } = props;
  const classes = useStyle();
  return (
    <Typography
      className={classes.containerMessageItemSender}
      variant="body1"
      fontWeight={600}
      title={userName}
    >
      {userName}
    </Typography>
  );
};

export default MessageItemSender;
