import { Typography, TypographyProps } from '@mui/material';
import classnames from 'classnames';
import { FC } from 'react';
import useStyle from './styles';

interface MessageItemTextProps {
  text: string;
}

const MessageItemText: FC<MessageItemTextProps & TypographyProps> = props => {
  const { text, className, ...otherProps } = props;
  const classes = useStyle();
  return (
    <Typography
      variant="body1"
      className={classnames(classes.containerMessageItemText)}
      {...otherProps}
    >
      {text}
    </Typography>
  );
};

export default MessageItemText;
