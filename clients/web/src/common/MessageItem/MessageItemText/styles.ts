import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerMessageItemText: {
    whiteSpace: 'pre-line',
  },
}));
