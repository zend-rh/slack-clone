import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerMessageItem: {
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
  },
  messageContainer: {
    marginLeft: 7,
    ...theme.containerStyles.flexColumn,
  },
  authorContainer: {
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
  },
  time: {
    paddingLeft: 10,
    marginTop: 2,
    color: 'gray',
  },
}));
