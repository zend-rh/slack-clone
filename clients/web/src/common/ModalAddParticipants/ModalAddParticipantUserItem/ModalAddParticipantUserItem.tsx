import { Add, Close } from '@mui/icons-material';
import { IconButton, ListItem, ListItemAvatar, ListItemText, Tooltip } from '@mui/material';
import { FC } from 'react';
import { globalSnackbarVar } from '../../../config/reactiveVars';
import {
  // useAddChannelParticipantsMutation,
  UserBasicInfosFragment,
  // useRemoveChannelParticipantsMutation,
} from '../../../generated/graphql';
import { getUserName } from '../../../utils/userUtils';
import { Avatar } from '../../Avatar';
import { Loading } from '../../Loading';
import useStyle from './styles';

interface ModalAddParticipantUserItemProps {
  user: UserBasicInfosFragment;
  isActive: boolean;
  channelId: string;
}

const ModalAddParticipantUserItem: FC<ModalAddParticipantUserItemProps> = props => {
  const { user, isActive, channelId } = props;
  const classes = useStyle();
  const isMe = true;
  const lastConnection = 0;
  return null;
  // const [mutationRemoveParticipant, { loading: removeParticipantsLoading }] =
  //   useRemoveChannelParticipantsMutation();
  // const [mutationAddParticipant, { loading: addParticipantLoading }] =
  //   useAddChannelParticipantsMutation();

  // const addParticipant = () => {
  //   if (!addParticipantLoading) {
  //     mutationAddParticipant({
  //       variables: {
  //         input: {
  //           channelId,
  //           userId: user.id,
  //         },
  //       },
  //     }).catch(() => {
  //       globalSnackbarVar({
  //         open: true,
  //         message: 'An error occured',
  //         type: 'ERROR',
  //       });
  //     });
  //   }
  // };

  // const removeParticipant = () => {
  //   if (!removeParticipantsLoading) {
  //     mutationRemoveParticipant({
  //       variables: {
  //         input: {
  //           channelId,
  //           userId: user.id,
  //         },
  //       },
  //     }).catch(() => {
  //       globalSnackbarVar({
  //         open: true,
  //         message: 'An error occured',
  //         type: 'ERROR',
  //       });
  //     });
  //   }
  // };

  // const isLoading = removeParticipantsLoading && addParticipantLoading;

  // return (
  //   <ListItem button={true} selected={isActive}>
  //     <ListItemAvatar>
  //       <Avatar
  //         indicatorStatus={isMe ? 'isMine' : lastConnection === 0 ? 'online' : 'offline'}
  //         className={classes.avatar}
  //         userName={getUserName(user)}
  //       />
  //     </ListItemAvatar>
  //     <ListItemText primary={getUserName(user)} />
  //     {!isActive && !isLoading && (
  //       <Tooltip title="Invite" enterDelay={200}>
  //         <IconButton className={classes.actionButton} onClick={addParticipant}>
  //           <Add color="primary" />
  //         </IconButton>
  //       </Tooltip>
  //     )}
  //     {isActive && !isLoading && !user.isMe && (
  //       <Tooltip title="Remove" enterDelay={200}>
  //         <IconButton className={classes.actionButton} onClick={removeParticipant}>
  //           <Close color="error" />
  //         </IconButton>
  //       </Tooltip>
  //     )}
  //     {isLoading && (
  //       <IconButton className={classes.actionButton}>
  //         <Loading size={35} />
  //       </IconButton>
  //     )}
  //   </ListItem>
  // );
};

export default ModalAddParticipantUserItem;
