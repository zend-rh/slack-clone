import { Dialog, DialogTitle, List } from '@mui/material';
import { FC } from 'react';
import { UserBasicInfosFragment } from '../../generated/graphql';
import { ModalAddParticipantUserItem } from './ModalAddParticipantUserItem';

interface ModalAddParticipantsProps {
  isOpen: boolean;
  onClose: () => void;
  users: UserBasicInfosFragment[];
  memberUsers: UserBasicInfosFragment[];
  channelId: string;
}

const ModalAddParticipants: FC<ModalAddParticipantsProps> = props => {
  const { isOpen, onClose, users, memberUsers, channelId } = props;
  const memberUsersIds = memberUsers.map(({ id }) => +id);
  return (
    <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" open={isOpen}>
      <DialogTitle id="simple-dialog-title">Manage participants</DialogTitle>
      <List>
        {users.map(user => (
          <ModalAddParticipantUserItem
            channelId={channelId}
            isActive={memberUsersIds.includes(+user.id)}
            user={user}
          />
        ))}
      </List>
    </Dialog>
  );
};

export default ModalAddParticipants;
