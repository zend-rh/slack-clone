import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerIndicator: {
    borderRadius: '50%',
    border: `${theme.colors.white} 2px solid`,
    backgroundColor: theme.colors.black,
  },
  containerIndicatorOnline: {
    border: `${theme.colors.black} 2px solid`,
    backgroundColor: theme.colors.jungleGreen,
  },
}));
