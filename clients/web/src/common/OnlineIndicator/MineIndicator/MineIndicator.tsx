import { useTheme } from '@mui/material';
import { FC } from 'react';

interface MineIndicatorProps {
  size: number;
}

const MineIndicator: FC<MineIndicatorProps> = props => {
  const { size } = props;
  const theme = useTheme();

  return (
    <div
      style={{
        borderBottom: `${size}px solid ${theme.colors.jungleGreen}`,
        borderLeft: `${size}px solid transparent`,
        width: 0,
        height: 0,
      }}
    />
  );
};

export default MineIndicator;
