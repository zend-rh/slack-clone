import classnames from 'classnames';
import { FC, HTMLAttributes } from 'react';
import { Indicator } from './Indicator';
import { MineIndicator } from './MineIndicator';
import useStyle from './styles';

interface OnlineIndicatorProps {
  status: 'isMine' | 'online' | 'offline';
  size?: number;
}

const OnlineIndicator: FC<OnlineIndicatorProps & HTMLAttributes<HTMLDivElement>> = props => {
  const { status, size: sizeProps, className, ...otherProps } = props;
  const size = sizeProps || 14;

  const classes = useStyle();
  return (
    <div
      {...otherProps}
      className={classnames(classes.containerOnlineIndicator, className)}
      style={{
        width: size,
        height: size,
      }}
    >
      {status !== 'isMine' ? (
        <Indicator size={size} isOffline={status === 'offline'} />
      ) : (
        <MineIndicator size={size} />
      )}
    </div>
  );
};

OnlineIndicator.defaultProps = {
  size: undefined,
};

export default OnlineIndicator;
