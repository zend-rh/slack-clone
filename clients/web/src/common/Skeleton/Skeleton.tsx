import { Skeleton as SkeletonMaterial } from '@mui/material';
import { withStyles } from '@mui/styles';

const Skeleton = withStyles({
  root: {
    backgroundColor: '#fff',
  },
})(SkeletonMaterial);

export default Skeleton;
