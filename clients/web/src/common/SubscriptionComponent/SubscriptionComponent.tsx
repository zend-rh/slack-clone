import { FC } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import {
  useSubscribeToNewChannelSubscription,
  useSubscribeToNewMessageSubscription,
  useSubscribeToUserStatusSubscription,
} from '../../generated/graphql';
import { updateCacheAfterSubscribsriptionChannelAdded } from '../../updateCache/channel';
import { updateCacheAfterSubscribsriptionMessageAdded } from '../../updateCache/message';
import { isChannelFn } from '../../utils/locationUtils';

const SubscriptionComponent: FC = () => {
  const history = useHistory();
  const { channelId, spaceId } = useParams<{ channelId?: string; spaceId?: string }>();
  const { pathname } = useLocation();
  const isChannel = isChannelFn(pathname);
  // User
  useSubscribeToUserStatusSubscription();
  // // Channel
  useSubscribeToNewChannelSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionChannelAdded,
    variables: {
      spaceId: spaceId || '',
    },
    skip: !spaceId,
  });
  // Message
  useSubscribeToNewMessageSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionMessageAdded,
    variables: {
      channelId: channelId || '',
    },
    skip: !channelId,
  });
  return null;
};

export default SubscriptionComponent;
