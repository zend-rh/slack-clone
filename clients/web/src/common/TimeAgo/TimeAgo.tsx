import { Typography, TypographyProps } from '@mui/material';
import formatDistance from 'date-fns/formatDistance';
import { fr } from 'date-fns/locale';
import { FC } from 'react';

interface TimeAgoProps {
  dateTime: Date;
}

const TimeAgo: FC<TimeAgoProps & TypographyProps> = props => {
  const { dateTime, ...otherProps } = props;
  const fromAgo = formatDistance(dateTime, new Date(), {
    locale: fr,
  });
  return (
    <Typography variant="caption" {...otherProps}>
      {fromAgo}
    </Typography>
  );
};

export default TimeAgo;
