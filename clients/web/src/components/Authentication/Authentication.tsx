import { Grid, Stack, Typography } from '@mui/material';
import classnames from 'classnames';
import { FC } from 'react';
import { Logo } from '../../common/Logo';
import { SlackLoginForm } from './SlackLoginForm';
import useStyle from './styles';

const Authentication: FC = () => {
  const classes = useStyle();

  return (
    <div className={classes.loginPageContainer}>
      <Grid container={true} lg={8} item={true} sm={12} className={classes.panelContainer}>
        <Grid container={true}>
          <Grid
            item={true}
            sm={12}
            xs={12}
            className={classnames(classes.gridItemContainer, classes.gridItemLeftContainer)}
          >
            <Stack
              direction="row"
              spacing={1}
              alignItems="center"
              justifyContent="center"
              marginBottom={3}
            >
              <Logo size={50} />
              <Typography color="black" align="center" variant="h4" fontWeight={600}>
                slack
              </Typography>
            </Stack>
            <Typography color="black" align="center" variant="h3" fontWeight={600}>
              Se connecter à Slack
            </Typography>
            <Typography color="black" align="center" variant="body1" fontWeight={500}>
              Nous vous suggérons d’utiliser <b>votre adresse e-mail professionnelle.</b>
            </Typography>
            <SlackLoginForm />
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default Authentication;
