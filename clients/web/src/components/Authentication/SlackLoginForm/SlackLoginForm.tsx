import { Link, Stack, TextField, Typography } from '@mui/material';
import { FC, KeyboardEvent, useState } from 'react';
import { CustomButton } from '../../../common/CustomButton';
import { useLoginMutation } from '../../../generated/graphql';
import authService from '../../../services/authServices';
import ErrorMessage from '../../../services/ErrorMessage';
import useStyle from './styles';

const SlackLoginForm: FC = () => {
  const classes = useStyle();

  const [username, setUsername] = useState<string | null>('');
  const [password, setPassword] = useState<string | null>('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [errorLog, setErrorLog] = useState('');

  const [loginMutation, { loading: isLoading }] = useLoginMutation();

  const handleLogin = () => {
    if (!isLoading && username?.trim() && password?.length) {
      setErrorLog('');
      loginMutation({
        variables: {
          email: username.trim(),
          password,
        },
      })
        .then(resp => {
          if (resp.data?.login) {
            authService.setAccessToken(resp.data.login.token);
            setIsLoggedIn(true);
          }
        })
        .catch(error => {
          if (error.networkError) {
            setErrorLog('Network error');
          } else if (error.graphQLErrors.length) {
            const firstError = error.graphQLErrors[0].message;
            if (
              [ErrorMessage.PASSWORD_INCORRECT, ErrorMessage.LOGIN_OR_PASSWORD_INCORRECT].includes(
                firstError
              )
            ) {
              setErrorLog('Email ou mot de passe incorrect');
            } else {
              setErrorLog('Internal server error');
            }
          }
        });
    }
  };

  const handleEnterPress = (evt: KeyboardEvent<HTMLDivElement>) => {
    if (evt.key === 'Enter') {
      handleLogin();
    }
  };

  if (isLoggedIn) {
    window.location.href = '/';
    return null;
  }

  return (
    <div className={classes.containerSlackLoginForm} onKeyDown={handleEnterPress}>
      <form onSubmit={handleLogin}>
        <div>
          <TextField
            onChange={evt => setUsername(evt.target.value || null)}
            value={username || ''}
            error={username === undefined}
            className={classes.inputText}
            placeholder="Email"
          />
          <TextField
            onChange={evt => setPassword(evt.target.value || null)}
            value={password || ''}
            error={password === undefined}
            className={classes.inputText}
            type="password"
            placeholder="Mot de passe"
            autoComplete=""
          />
        </div>
        <h5 className={classes.errorMessage}>{errorLog}</h5>
        <CustomButton
          onClick={handleLogin}
          className={classes.buttonSubmit}
          disabled={!password || !username}
          isLoading={isLoading}
        >
          Connexion
        </CustomButton>
        <Stack marginTop={2} direction="row" alignItems="center" spacing={1}>
          <Typography color="gray" align="center" variant="caption">
            Vous découvrez Slack ?
          </Typography>
          <Link href="/signup">
            <Typography color="blue" align="center" variant="body2">
              Créer un compte
            </Typography>
          </Link>
        </Stack>
      </form>
    </div>
  );
};

export default SlackLoginForm;
