import { Stack, Typography } from '@mui/material';
import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import CustomBUtton from '../../../common/CustomButton/CustomButton';
import useStyle from './styles';

const CreateSpace: FC = () => {
  const classes = useStyle();
  const history = useHistory();
  return (
    <div className={classes.container}>
      <div className={classes.body}>
        <Stack direction="row" alignItems="center" justifyContent="space-between">
          <Typography color="black" align="center" variant="body1" fontWeight={600}>
            Vous souhaitez utiliser Slack avec une équipe différente ?
          </Typography>
          <CustomBUtton color="secondary" variant="outlined" onClick={() => history.push('/setup')}>
            Créer un nouvel espace de travail
          </CustomBUtton>
        </Stack>
      </div>
    </div>
  );
};

export default CreateSpace;
