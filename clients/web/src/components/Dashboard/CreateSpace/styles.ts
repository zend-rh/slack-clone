import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { important } from '../../../utils/materialUtils';

export default makeStyles((theme: Theme) => ({
  container: {
    margin: 'auto',
    marginTop: 50,
    border: '5px solid #732A6F',
    borderRadius: 10,
  },
  head: {
    borderRadius: '5px 5px 5px 5px',
    padding: 15,
    backgroundColor: '#ecdeec',
  },
  body: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: '5px 5px 5px 5px',
  },

  buttonSubmit: {
    marginTop: 110,
    width: '100%',
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: 20,
    fontWeight: 600,
    backgroundColor: `${theme.colors.grape}!important`,
    [theme.breakpoints.down('xs')]: {
      marginTop: 77,
    },
  },
}));
