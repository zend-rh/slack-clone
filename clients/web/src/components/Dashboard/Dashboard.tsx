import { Grid, Stack, Typography } from '@mui/material';
import classnames from 'classnames';
import { FC } from 'react';
import hand from '../../assets/waving-hand.gif';
import { useMeQuery } from '../../generated/graphql';
import { getUserName } from '../../utils/userUtils';
import { CreateSpace } from './CreateSpace';
import { SpaceList } from './SpaceList';
import useStyle from './styles';

const Dashboard: FC = () => {
  const classes = useStyle();
  const { data: dataMe } = useMeQuery();
  return (
    <div className={classes.container}>
      <Grid container={true} lg={10} item={true} sm={12} className={classes.panelContainer}>
        <Grid container={true}>
          <Grid
            item={true}
            sm={12}
            xs={12}
            className={classnames(classes.gridItemContainer, classes.gridItemLeftContainer)}
          >
            <Stack direction="row" spacing={1} alignItems="center" marginBottom={3}>
              <img src={hand as any} alt="hand" width={50} height={50} />
              <Typography color="white" align="center" variant="h4" fontWeight={600}>
                Ravi de vous revoir {getUserName(dataMe?.me as any)}
              </Typography>
            </Stack>
            <SpaceList />
            <CreateSpace />
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default Dashboard;
