import { Link, Stack, TextField, Typography } from '@mui/material';
import { FC, KeyboardEvent, useState } from 'react';
import { useHistory } from 'react-router-dom';
import logo from '../../../assets/space.png';
import { CustomButton } from '../../../common/CustomButton';
import CustomBUtton from '../../../common/CustomButton/CustomButton';
import { useGetMySpacesQuery, useLoginMutation, useMeQuery } from '../../../generated/graphql';
import authService from '../../../services/authServices';
import ErrorMessage from '../../../services/ErrorMessage';
import useStyle from './styles';

const SpaceList: FC = () => {
  const classes = useStyle();
  const history = useHistory();
  const { data } = useMeQuery();
  const { data: dataSpaces, loading } = useGetMySpacesQuery();
  if (!loading && dataSpaces?.me?.joinSpaces.length === 0) return null;
  return (
    <div className={classes.container}>
      <div className={classes.head}>
        <Typography>Espaces de travail pour {data?.me?.email}</Typography>
      </div>
      <div className={classes.body}>
        {(dataSpaces?.me?.joinSpaces || []).map(e => (
          <Stack
            marginTop={1}
            marginBottom={1}
            direction="row"
            spacing={1}
            justifyContent="space-between"
            alignItems="center"
          >
            <Stack direction="row" spacing={1} alignItems="center">
              <img src={logo} alt="logo" width={50} height={50} />
              <Stack direction="column">
                <Typography color="black" variant="body1" fontWeight={600}>
                  {e.name}
                </Typography>
                <Typography color="gray" variant="caption">
                  {`${e.members?.length} membre(s)`}
                </Typography>
              </Stack>
            </Stack>
            <CustomButton
              onClick={() => history.push(`/space/${e.id}/channel`)}
              className={classes.buttonSubmit}
            >
              Lancer slack
            </CustomButton>
          </Stack>
        ))}
      </div>
    </div>
  );
};

export default SpaceList;
