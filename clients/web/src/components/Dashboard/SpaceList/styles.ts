import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { important } from '../../../utils/materialUtils';

export default makeStyles((theme: Theme) => ({
  container: {
    margin: 'auto',
    marginTop: 50,
    border: '5px solid #732A6F',
    borderRadius: 10,
  },
  head: {
    borderRadius: '5px 5px 0 0',
    padding: 15,
    backgroundColor: '#ecdeec',
  },
  body: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: '0 0 5px 5px ',
  },
  inputText: {
    width: '100%',
    color: theme.colors.black,
    '&:last-child': {
      [theme.breakpoints.down('xs')]: {
        marginTop: 15,
      },
      marginTop: 15,
    },
  },
  buttonSubmit: {
    marginTop: 110,
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: 20,
    fontWeight: 600,
    backgroundColor: `${theme.colors.grape}!important`,
    [theme.breakpoints.down('xs')]: {
      marginTop: 77,
    },
  },
  errorMessage: {
    textAlign: 'center',
    margin: 0,
    marginTop: 7,
    fontSize: 14,
    fontWeight: 'normal',
    color: theme.colors.red,
    minHeight: 30,
  },
}));
