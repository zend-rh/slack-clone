import { FC } from 'react';
import { ContainerMessages } from './ContainerMessages';
import useStyle from './styles';

const Center: FC = () => {
  const classes = useStyle();
  return (
    <div className={classes.containerCenter}>
      <ContainerMessages />
    </div>
  );
};

export default Center;
