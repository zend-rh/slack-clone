import { FC } from 'react';
import { ErrorComponent } from '../../../../../common/ErrorComponent';
import { Loading } from '../../../../../common/Loading';
import { useGetChannelMessagesQuery } from '../../../../../generated/graphql';
import { Discussion } from '../Discussion';
import useStyle from './styles';

interface ChanelMessageLoaderProps {
  channelId: string;
  isChannel: boolean;
}

const ChanelMessageLoader: FC<ChanelMessageLoaderProps> = props => {
  const { channelId, isChannel } = props;
  const { data, loading, error } = useGetChannelMessagesQuery({
    variables: {
      id: channelId.toString(),
    },
  });

  const messages = data?.channel?.messages || [];
  const channelName = data?.channel?.name;
  const classes = useStyle();
  return (
    <div className={classes.containerChannelMessageLoader}>
      {loading && <Loading isFullHeight={true} />}
      {!loading && error && <ErrorComponent error={error} isFullWidth={true} />}
      {!loading && !error && channelName && (
        <Discussion
          isOnline={false}
          messages={(messages || []) as any}
          isChannel={isChannel}
          title={channelName}
        />
      )}
    </div>
  );
};

export default ChanelMessageLoader;
