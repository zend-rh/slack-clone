import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerChannelMessageLoader: {
    flex: 1,
  },
}));
