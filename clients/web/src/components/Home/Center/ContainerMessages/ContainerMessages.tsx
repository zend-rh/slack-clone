import { Typography } from '@mui/material';
import { FC } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { isChannelFn } from '../../../../utils/locationUtils';
import { ChannelMessageLoader } from './ChannelMessageLoader';
import useStyle from './styles';

const ContainerMessages: FC = () => {
  const classes = useStyle();
  const { pathname } = useLocation();
  const { channelId } = useParams<{ channelId?: string }>();

  const isChannel = isChannelFn(pathname);
  return (
    <div className={classes.containerMessages}>
      {channelId && <ChannelMessageLoader channelId={channelId} isChannel={isChannel} />}
      {!channelId && (
        <div className={classes.emptyText}>
          <Typography>Select one item from the left</Typography>
        </div>
      )}
    </div>
  );
};

export default ContainerMessages;
