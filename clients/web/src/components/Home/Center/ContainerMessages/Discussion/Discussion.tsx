import { FC } from 'react';
import { ListMessage } from '../../../../../common/ListMessage';
import { MessageInfosFragment } from '../../../../../generated/graphql';
import { MessageBox } from '../../MessageBox';
import useStyle from './styles';
import { TitleMessage } from './TitleMessage';

interface DiscussionProps {
  title: string;
  isChannel: boolean;
  messages: MessageInfosFragment[];
  isOnline: boolean;
}

const Discussion: FC<DiscussionProps> = props => {
  const { title, isChannel, messages, isOnline } = props;
  const classes = useStyle();
  return (
    <div className={classes.containerDiscussion}>
      <TitleMessage isOnline={isOnline} title={title} isChannel={isChannel} />
      <ListMessage messages={messages} />
      <MessageBox />
    </div>
  );
};

export default Discussion;
