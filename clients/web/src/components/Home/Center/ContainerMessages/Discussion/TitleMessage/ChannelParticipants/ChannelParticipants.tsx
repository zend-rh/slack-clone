import { Edit } from '@mui/icons-material';
import { IconButton, Typography } from '@mui/material';
import { FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import Avatar from '../../../../../../../common/Avatar/Avatar';
import { Loading } from '../../../../../../../common/Loading';
import ModalAddParticipants from '../../../../../../../common/ModalAddParticipants/ModalAddparticipants';
import {
  useGetChannelMembersQuery,
  useGetSpaceMembersQuery,
  useMeQuery,
} from '../../../../../../../generated/graphql';
import { getUserName } from '../../../../../../../utils/userUtils';
import useStyle from './styles';

interface ChannelParticipantsProps {
  channelId: string;
}

const ChannelParticipants: FC<ChannelParticipantsProps> = props => {
  const { channelId } = props;
  const [isOpen, setIsOpen] = useState(false);
  const { spaceId } = useParams<{ spaceId?: string }>();
  const classes = useStyle();
  const { data: dataMe } = useMeQuery();
  const { data, loading } = useGetChannelMembersQuery({
    variables: {
      id: channelId,
    },
  });

  const { data: userData, loading: userLoading } = useGetSpaceMembersQuery({
    variables: {
      id: spaceId || '',
    },
    skip: !spaceId,
  });

  if (loading) {
    return (
      <div className={classes.containerChannelParticipants}>
        <Loading size={25} />
      </div>
    );
  }

  const allUsers = userData?.space?.members || [];

  const users = data?.channel?.members || [];

  const isAuthor = Boolean(data?.channel?.authorId === dataMe?.me?.id);

  const isPrivate = Boolean(data?.channel?.isPrivate);

  if (!isPrivate) {
    return (
      <div className={classes.containerChannelParticipants}>
        <Typography color="error" variant="body2">
          Public channel
        </Typography>
      </div>
    );
  }

  return (
    <div className={classes.containerChannelParticipants}>
      {allUsers.map(
        user =>
          user && (
            <Avatar
              userName={getUserName(user)}
              indicatorStatus={
                user.id === dataMe?.me?.id
                  ? 'isMine'
                  : // : user.lastConnection === 0
                  user?.isOnline
                  ? 'online'
                  : 'offline'
              }
              isCircle={true}
            />
          )
      )}
      {Boolean(isAuthor) && (
        <>
          <IconButton onClick={() => setIsOpen(true)}>
            <Edit />
          </IconButton>
          <ModalAddParticipants
            channelId={channelId.toString()}
            memberUsers={users}
            users={allUsers}
            isOpen={isOpen}
            onClose={() => setIsOpen(false)}
          />
        </>
      )}
    </div>
  );
};

export default ChannelParticipants;
