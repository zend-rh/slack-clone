import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerChannelParticipants: {
    marginLeft: 'auto',
  },
}));
