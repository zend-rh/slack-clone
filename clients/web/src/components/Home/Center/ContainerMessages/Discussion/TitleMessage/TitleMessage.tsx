import { Divider, Typography } from '@mui/material';
import { FC, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { Avatar } from '../../../../../../common/Avatar';
import { useGetSpaceChannelsQuery } from '../../../../../../generated/graphql';
import { getUserName } from '../../../../../../utils/userUtils';
import { ChannelParticipants } from './ChannelParticipants';
import useStyle from './styles';

interface TitleMessageProps {
  title: string;
  isChannel: boolean;
  isOnline: boolean;
}

const TitleMessage: FC<TitleMessageProps> = props => {
  const { title, isChannel, isOnline } = props;
  const classes = useStyle();
  const { channelId, spaceId } = useParams<{ channelId: string; spaceId?: string }>();
  const { data } = useGetSpaceChannelsQuery({
    variables: {
      id: spaceId || '',
    },
    fetchPolicy: 'cache-first',
    skip: !spaceId,
  });
  const currentChatDirect = useMemo(() => {
    if (data?.space?.directChannels) {
      return data?.space?.directChannels.find(e => e?.channel?.id === channelId);
    }
    return undefined;
  }, [channelId, data?.space?.directChannels]);

  const currentUserNameChannel = currentChatDirect?.user
    ? getUserName(currentChatDirect?.user)
    : '';
  return (
    <div className={classes.containerTitleMessage}>
      <div className={classes.titleContainer}>
        {!isChannel && currentChatDirect?.user && (
          <Avatar
            indicatorStatus={currentChatDirect?.user?.isOnline ? 'online' : 'offline'}
            size={35}
            userName={currentUserNameChannel}
          />
        )}
        <Typography variant="h5">
          {isChannel && <span className={classes.title}>#</span>}
          <span className={classes.title}>{isChannel ? title : currentUserNameChannel}</span>
        </Typography>
        {isChannel && <ChannelParticipants channelId={channelId} />}
      </div>
      <Divider className={classes.divider} />
    </div>
  );
};

export default TitleMessage;
