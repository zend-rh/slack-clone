import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerTitleMessage: {},
  title: {
    marginLeft: 7,
    fontSize: 18,
    fontWeight: 600,
  },
  titleContainer: {
    padding: '14px 14px',
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
  },
  divider: {
    flex: 1,
  },
}));
