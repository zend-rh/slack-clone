import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerDiscussion: {
    height: 'calc(100vh - 48px)',
    ...theme.containerStyles.flexColumn,
  },
}));
