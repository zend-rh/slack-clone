import { FC } from 'react';
import { ErrorComponent } from '../../../../../common/ErrorComponent';
import { Loading } from '../../../../../common/Loading';
// import { useGetUserMessageQuery } from '../../../../../generated/graphql';
import { getUserName } from '../../../../../utils/userUtils';
import { Discussion } from '../Discussion';
import useStyle from './styles';

interface DiscussionMessageLoaderProps {
  userId: string;
}

const DiscussionMessageLoader: FC<DiscussionMessageLoaderProps> = props => {
  const { userId } = props;
  return null;
  // const { data, loading, error } = useGetUserMessageQuery({
  //   variables: {
  //     filter: {
  //       userId: userId.toString(),
  //     },
  //   },
  // });

  // const messageData = data?.me.messages;
  // const messages = messageData?.messages || [];
  // const user = messageData?.user;
  // const title = user ? getUserName(user) : 'TODO: title';

  // const classes = useStyle();
  // return (
  //   <div className={classes.containerDiscussionMessageLoader}>
  //     {loading && <Loading isFullHeight={true} />}
  //     {error && <ErrorComponent error={error} isFullWidth={true} />}
  //     {!loading && !error && (
  //       <Discussion
  //         isOnline={user?.lastConnection === 0}
  //         messages={messages}
  //         isChannel={false}
  //         title={title}
  //       />
  //     )}
  //   </div>
  // );
};

export default DiscussionMessageLoader;
