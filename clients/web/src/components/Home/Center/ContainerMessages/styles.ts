import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerMessages: {
    flex: 1,
  },
  emptyText: {
    ...theme.containerStyles.flexColumn,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
}));
