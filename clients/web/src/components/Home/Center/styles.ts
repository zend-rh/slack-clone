import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerCenter: {
    flex: 1,
    backgroundColor: theme.colors.white,
    ...theme.containerStyles.flexColumn,
  },
}));
