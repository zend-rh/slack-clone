import { Stack, Typography } from '@mui/material';
import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { Logo } from '../../../common/Logo';
import useStyle from './styles';
import { UserAvatar } from './userAvatar';

interface HeaderProps {
  userFullName: string;
}

const Header: FC<HeaderProps> = props => {
  const { userFullName } = props;
  const classes = useStyle();
  const history = useHistory();

  return (
    <div className={classes.containerHeader}>
      <div onClick={() => history.push('/')} className={classes.logo}>
        <Stack
          direction="row"
          marginLeft={2}
          spacing={1}
          alignItems="center"
          justifyContent="center"
        >
          <Logo size={25} />
          <Typography variant="h5" className={classes.title} fontWeight={600}>
            slack
          </Typography>
        </Stack>
      </div>

      <UserAvatar userFullName={userFullName} />
    </div>
  );
};

export default Header;
