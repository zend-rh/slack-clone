import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { FC, useEffect, useState } from 'react';
import { Avatar } from '../../../../common/Avatar';
import { CustomButton } from '../../../../common/CustomButton';
import { globalSnackbarVar } from '../../../../config/reactiveVars';
import { useMeQuery, useUpdateUserMutation } from '../../../../generated/graphql';
import { getUserName } from '../../../../utils/userUtils';

interface UserInfosProps {
  isOpen: boolean;
  close: () => void;
}

const UserInfos: FC<UserInfosProps> = props => {
  const { isOpen, close } = props;

  const { data } = useMeQuery();

  const user = data?.me;

  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [phone, setPhone] = useState('');

  const [mutationUpdateUser, { loading }] = useUpdateUserMutation();

  useEffect(() => {
    if (user) {
      setFirstname(user?.firstName || '');
      setLastname(user?.lastName || '');
      setPhone('');
    }
  }, [user]);

  if (!user) {
    return null;
  }

  const onUpdate = () => {
    if (data.me?.id)
      mutationUpdateUser({
        variables: {
          id: data.me?.id,
          input: {
            displayName: data.me?.displayName || '',
            email: data.me?.email || '',
            firstName: firstname.trim(),
            lastName: lastname.trim(),
          },
        },
      })
        .then(() => {
          globalSnackbarVar({
            open: true,
            message: 'Success',
            type: 'SUCCESS',
          });
          close();
        })
        .catch(() => {
          globalSnackbarVar({
            open: true,
            message: 'An error occured',
            type: 'ERROR',
          });
        });
  };

  const editEnabled = firstname !== user?.firstName || lastname !== user?.lastName;

  return (
    <div>
      <Dialog open={isOpen} onClose={close} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Profil</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {/* <Avatar userName={getUserName(user)} indicatorStatus="isMine" /> {user?.displayName} */}
          </DialogContentText>
          <TextField
            onChange={evt => setFirstname(evt.target.value)}
            margin="dense"
            value={firstname}
            label="Firstname"
            fullWidth={true}
            error={!firstname.trim()}
          />
          <TextField
            onChange={evt => setLastname(evt.target.value)}
            margin="dense"
            value={lastname}
            label="LastName"
            fullWidth={true}
            error={!lastname.trim()}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Annuler
          </Button>
          <CustomButton disabled={!editEnabled || loading} onClick={onUpdate} color="primary">
            Enregistrer
          </CustomButton>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default UserInfos;
