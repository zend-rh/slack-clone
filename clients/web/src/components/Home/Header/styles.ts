import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerHeader: {
    zIndex: theme.zIndex.appBar,
    backgroundColor: theme.colors.valentino,
    width: '100%',
    position: 'relative',
    boxShadow: theme.boxShadow.light,
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 4,
    paddingBottom: 4,
  },
  title: {
    marginLeft: 'auto',
    color: theme.colors.white,
  },
  avatar: {
    marginLeft: 'auto',
    marginRight: 7,
  },
  logo: {
    cursor: 'pointer',
  },
}));
