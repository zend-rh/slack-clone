import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerUserAvatar: {
    marginLeft: 'auto',
    marginRight: 7,
  },
  avatar: {},
  popover: {
    ...theme.containerStyles.flexRow,
  },
  listItem: {
    minWidth: 200,
  },
}));
