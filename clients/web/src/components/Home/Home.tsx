import { FC } from 'react';
import { ErrorComponent } from '../../common/ErrorComponent';
import { Loading } from '../../common/Loading';
import { SubscriptionComponent } from '../../common/SubscriptionComponent';
import { useMeQuery } from '../../generated/graphql';
import { getUserName } from '../../utils/userUtils';
import Center from './Center/Center';
import { Header } from './Header';
import { Left } from './Left';
import useStyle from './styles';

const Home: FC = () => {
  const classes = useStyle();
  const { data, loading, error } = useMeQuery();

  if (error || loading) {
    return (
      <div className={classes.containerHome}>
        {!loading && error && <ErrorComponent error={error} isFullWidth={true} />}
        {loading && <Loading isFullHeight={true} />}
      </div>
    );
  }

  const user = data?.me;

  if (!user) {
    return null;
  }

  return (
    <>
      <div className={classes.containerHome}>
        <Header userFullName={getUserName(user)} />
        <div className={classes.containerHomeBody}>
          <Left />
          <Center />
        </div>
      </div>
      <SubscriptionComponent />
    </>
  );
};

export default Home;
