import { Add } from '@mui/icons-material';
import { FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import { CustomButton } from '../../../../../common/CustomButton';
import { DialogAddChannel } from './DialogAddChannel';
import useStyle from './styles';

const AddChannel: FC = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const classes = useStyle();

  const { spaceId } = useParams<{ spaceId?: string }>();
  return (
    <>
      <div onClick={() => setIsDialogOpen(true)} className={classes.containerAddChannel}>
        <Add fontSize="small" />
        <span className={classes.btnTxt}>Ajouter des canaux</span>
      </div>
      <DialogAddChannel
        isOpen={isDialogOpen}
        close={() => setIsDialogOpen(false)}
        spaceId={spaceId || ''}
      />
    </>
  );
};

export default AddChannel;
