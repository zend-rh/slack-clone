import { FormControlLabel, Switch } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { FC, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useCreateChannelMutation } from '../../../../../../generated/graphql';
import { updateCacheAfterAddingChannel } from '../../../../../../updateCache/channel';

interface DialogAddChannelProps {
  spaceId: string;
  isOpen: boolean;
  close: () => void;
}

const DialogAddChannel: FC<DialogAddChannelProps> = props => {
  const { spaceId, isOpen: open, close } = props;

  const [name, setName] = useState('');
  const [isPrivate, setIsPrivate] = useState(false);

  useEffect(() => {
    if (open) {
      setName('');
      setIsPrivate(false);
    }
  }, [open]);

  const [addChannelMutation, { loading }] = useCreateChannelMutation({
    update: updateCacheAfterAddingChannel,
  });

  const history = useHistory();

  const onSubmit = () => {
    if (!loading && name.trim()) {
      addChannelMutation({
        variables: {
          inputCreateChannel: {
            isPrivate,
            name: name.trim(),
            spaceId,
          },
        },
      }).then(({ data }) => {
        if (data?.createChannel) {
          history.push(`/space/${spaceId}/channel/${data.createChannel.id}`);
          close();
        }
      });
    }
  };

  return (
    <div>
      <Dialog open={open} onClose={close} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Créer un cannal</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Les canaux permettent à votre équipe de communiquer. Leur utilisation est optimale
            lorsqu’ils sont organisés autour d’un thème (#marketing, par exemple). .
          </DialogContentText>
          <TextField
            autoFocus={true}
            margin="dense"
            id="name"
            value={name}
            onChange={evt => setName(evt.target.value)}
            label="Name"
            type="text"
            prefix="# "
            placeholder="par exemple, plan-budget"
            fullWidth={true}
          />
          <FormControlLabel
            control={
              <Switch checked={isPrivate} onChange={(evt, checked) => setIsPrivate(checked)} />
            }
            label="Privé"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Annuler
          </Button>
          <Button disabled={!name.trim()} onClick={onSubmit} color="primary">
            Créer
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DialogAddChannel;
