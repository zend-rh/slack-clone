import ArrowDropDownOutlinedIcon from '@mui/icons-material/ArrowDropDownOutlined';
import ArrowDropUpOutlinedIcon from '@mui/icons-material/ArrowDropUpOutlined';
import { IconButton, List, Stack, Typography } from '@mui/material';
import { FC, useEffect, useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { ChannelMenuItem } from '../../../../common/ChannelMenuItem';
import { ErrorComponent } from '../../../../common/ErrorComponent';
import Skeleton from '../../../../common/Skeleton/Skeleton';
import { useGetSpaceChannelsQuery } from '../../../../generated/graphql';
import { isChannelFn } from '../../../../utils/locationUtils';
import { AddChannel } from './AddChannel';
import useStyle from './styles';

const ChannelsSection: FC = () => {
  const classes = useStyle();
  const [close, setClose] = useState(false);
  const { channelId, spaceId } = useParams<{ spaceId?: string; channelId?: string }>();
  const { pathname } = useLocation();

  const isChannel = isChannelFn(pathname);

  const { data, loading, error } = useGetSpaceChannelsQuery({
    variables: {
      id: spaceId ?? '',
    },
    skip: !spaceId,
  });

  const channels = data?.space?.channels || [];
  const firstChannelId = channels.length ? channels[0]?.id : null;

  const history = useHistory();

  useEffect(() => {
    if (!channelId && firstChannelId && spaceId) {
      history.replace(`/space/${spaceId}/channel/${firstChannelId}`);
    }
  }, [firstChannelId, channelId, history, spaceId]);

  if (!spaceId || (!loading && !data)) {
    return null;
  }
  return (
    <div className={classes.containerChannelsSection}>
      <Stack direction="row" alignItems="center">
        <IconButton onClick={() => setClose(!close)}>
          {!close ? (
            <ArrowDropDownOutlinedIcon
              style={{
                color: '#D1BFCF',
              }}
              fontSize="small"
            />
          ) : (
            <ArrowDropUpOutlinedIcon
              style={{
                color: '#D1BFCF',
              }}
              fontSize="small"
            />
          )}
        </IconButton>
        <Typography className={classes.title} variant="body2" color="#D1BFCF">
          Cannaux
        </Typography>
      </Stack>

      {error && <ErrorComponent error={error} />}
      {loading && (
        <>
          <Skeleton height={35} />
          <Skeleton height={35} />
          <Skeleton height={35} />
        </>
      )}
      {!loading && (
        <>
          <List>
            {channels
              .filter(i =>
                close ? Boolean(isChannel && i && i.id && channelId && i.id === channelId) : true
              )
              .map(
                channel =>
                  channel && (
                    <ChannelMenuItem
                      isActive={Boolean(
                        isChannel && channel.id && channelId && channel.id === channelId
                      )}
                      name={channel.name}
                      isPrivate={channel.isPrivate || false}
                      id={channel.id}
                      key={channel.id}
                      spaceId={spaceId}
                    />
                  )
              )}
          </List>
          <AddChannel />
        </>
      )}
    </div>
  );
};

export default ChannelsSection;
