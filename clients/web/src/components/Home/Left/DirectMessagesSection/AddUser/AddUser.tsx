import { Add } from '@mui/icons-material';
import { FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import { DialogAddUser } from './DialogAddUser';
import useStyle from './styles';

const AddUser: FC = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const classes = useStyle();

  const { spaceId } = useParams<{ spaceId?: string }>();
  return (
    <>
      <div onClick={() => setIsDialogOpen(true)} className={classes.containerAddChannel}>
        <Add fontSize="small" />
        <span className={classes.btnTxt}>Ajouter des collaborateurs</span>
      </div>
      <DialogAddUser
        isOpen={isDialogOpen}
        close={() => setIsDialogOpen(false)}
        spaceId={spaceId || ''}
      />
    </>
  );
};

export default AddUser;
