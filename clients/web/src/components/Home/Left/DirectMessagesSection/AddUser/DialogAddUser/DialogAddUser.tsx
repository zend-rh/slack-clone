import { Stack } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { FC, useState } from 'react';
import { CustomButton } from '../../../../../../common/CustomButton';
import {
  useGetSpaceInfoQuery,
  useInviteUserToSpaceMutation,
} from '../../../../../../generated/graphql';
import useStyles from './styles';

interface DialogAddUserProps {
  spaceId: string;
  isOpen: boolean;
  close: () => void;
}

const DialogAddUser: FC<DialogAddUserProps> = props => {
  const { spaceId, isOpen: open, close } = props;
  const { data: dataSpace } = useGetSpaceInfoQuery({
    variables: {
      id: spaceId,
    },
  });
  const [emails, setEmails] = useState(['']);
  const classes = useStyles();

  const [inviteUser, { loading }] = useInviteUserToSpaceMutation({});

  const data = emails.filter(e => e.length);
  const onSubmit = () => {
    if (data.length > 0) {
      inviteUser({
        variables: {
          inputCreateInviteUser: {
            id: spaceId,
            usersToEmail: data,
          },
        },
      }).then(({ data: dataRes }) => {
        if (dataRes?.createInviteUser) {
          setEmails(['']);
          close();
        }
      });
    }
  };

  return (
    <div>
      <Dialog open={open} onClose={close} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          Inviter des personnes à rejoindre {dataSpace?.space?.name}
        </DialogTitle>
        <DialogContent>
          {/* <DialogContentText></DialogContentText> */}
          <Stack direction="column" spacing={1}>
            {emails.map((user, i) => (
              <TextField
                key={i.toString()}
                onChange={evt => {
                  const clone = [...emails];
                  clone[i] = evt.target.value || '';
                  setEmails(clone);
                }}
                value={emails[i] || ''}
                placeholder="Ex: ellis@gmail.com"
              />
            ))}
            <span
              onClick={() => {
                setEmails([...emails, '']);
              }}
              className={classes.addUser}
            >
              Ajouter une autre personne
            </span>
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Annuler
          </Button>
          <CustomButton
            isLoading={loading}
            disabled={data.length === 0}
            onClick={onSubmit}
            color="primary"
          >
            Envoyez
          </CustomButton>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DialogAddUser;
