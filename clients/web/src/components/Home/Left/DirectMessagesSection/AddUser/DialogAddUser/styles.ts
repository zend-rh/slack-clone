import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerDialogAddChannel: {},
  inputText: {
    width: '100%',
    color: 'black',
  },
  addUser: {
    color: 'blue',
    '&:hover': {
      textDecoration: 'underline',
      cursor: 'pointer',
    },
  },
}));
