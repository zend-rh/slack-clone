import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  containerAddChannel: {
    paddingLeft: 18,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: 'transparent!important',
    boxShadow: 'none!important',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    color: '#D1BFCF',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#3C0037!important',
    },
  },
  btnTxt: {
    marginLeft: 13,
    fontSize: 14,
  },
}));
