import ArrowDropDownOutlinedIcon from '@mui/icons-material/ArrowDropDownOutlined';
import ArrowDropUpOutlinedIcon from '@mui/icons-material/ArrowDropUpOutlined';
import { IconButton, List, Stack, Typography } from '@mui/material';
import { FC, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { DirectMessageMenuItem } from '../../../../common/DirectMessageMenuItem';
import { Skeleton } from '../../../../common/Skeleton';
import { useGetSpaceChannelsQuery, useMeQuery } from '../../../../generated/graphql';
import { isChannelFn } from '../../../../utils/locationUtils';
import { getUserName } from '../../../../utils/userUtils';
import { AddUser } from './AddUser';
import useStyle from './styles';

const DirectMessagesSection: FC = () => {
  const classes = useStyle();

  const [close, setClose] = useState(false);
  const { spaceId, channelId } = useParams<{ spaceId?: string; channelId?: string }>();
  const { pathname } = useLocation();

  const isChannel = isChannelFn(pathname);
  const { data, error, loading } = useGetSpaceChannelsQuery({
    variables: {
      id: spaceId || '',
    },
    skip: !spaceId,
  });

  const usersChannels = data?.space?.directChannels || [];

  return (
    <div className={classes.containerDirectMessagesSection}>
      <Stack direction="row" alignItems="center">
        <IconButton onClick={() => setClose(!close)}>
          {!close ? (
            <ArrowDropDownOutlinedIcon
              style={{
                color: '#D1BFCF',
              }}
              fontSize="small"
            />
          ) : (
            <ArrowDropUpOutlinedIcon
              style={{
                color: '#D1BFCF',
              }}
              fontSize="small"
            />
          )}
        </IconButton>
        <Typography className={classes.title} variant="body2" color="#D1BFCF">
          Messages directs
        </Typography>
      </Stack>
      <List>
        {error && <Typography color="error">Error</Typography>}
        {loading && (
          <>
            <Skeleton height={35} />
            <Skeleton height={35} />
            <Skeleton height={35} />
          </>
        )}
        {!loading &&
          usersChannels
            .filter(i =>
              i && i.channel && i.user && close ? i.channel.id && i.channel.id === channelId : true
            )
            .map(
              userC =>
                userC &&
                userC.channel &&
                userC.user && (
                  <DirectMessageMenuItem
                    spaceId={spaceId || ''}
                    isActive={Boolean(!isChannel && channelId && userC.channel.id === channelId)}
                    userName={getUserName(userC.user)}
                    channelId={userC.channel.id}
                    key={userC.id}
                    isOnline={Boolean(userC.user?.isOnline)}
                    // isOnline={user.lastConnection === 0}
                  />
                )
            )}
      </List>
      <AddUser />
    </div>
  );
};

export default DirectMessagesSection;
