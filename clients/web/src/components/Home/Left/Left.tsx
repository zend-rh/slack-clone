import { FC } from 'react';
import { ChannelsSection } from './ChannelsSection';
import { DirectMessagesSection } from './DirectMessagesSection';
import { Space } from './Space';
import useStyle from './styles';

const Left: FC = () => {
  const classes = useStyle();
  return (
    <div className={classes.containerLeft}>
      <Space />
      <div className={classes.centerSection}>
        <ChannelsSection />
        <DirectMessagesSection />
      </div>
    </div>
  );
};

export default Left;
