import { Typography } from '@mui/material';
import { FC } from 'react';
import { useParams } from 'react-router-dom';
import { useGetSpaceInfoQuery } from '../../../../generated/graphql';
import useStyle from './styles';

const Space: FC = () => {
  const classes = useStyle();
  const { spaceId } = useParams<{ spaceId?: string }>();
  const { data } = useGetSpaceInfoQuery({
    variables: {
      id: spaceId || '',
    },
    skip: !spaceId,
  });
  if (!data?.space) {
    return null;
  }
  const { name } = data.space;
  return (
    <div className={classes.containerUserSection}>
      <Typography color="white" variant="h6" fontWeight={600}>
        {name}
      </Typography>
    </div>
  );
};

export default Space;
