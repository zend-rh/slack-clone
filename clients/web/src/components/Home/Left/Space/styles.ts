import { Theme } from '@mui/material/styles/createTheme';
import { makeStyles } from '@mui/styles';

export default makeStyles<Theme>(() => ({
  containerUserSection: {
    padding: 14,
    boxShadow: '0 1px 0 0 rgb(255 255 255 / 10%)',
  },
}));
