import { Typography } from '@mui/material';
import { FC } from 'react';
import { useMeQuery } from '../../../../generated/graphql';
import useStyle from './styles';

const UserSection: FC = () => {
  const classes = useStyle();
  const { data } = useMeQuery();
  const user = data?.me;
  if (!user) {
    return null;
  }
  const { firstName, displayName } = user;
  return (
    <div className={classes.containerUserSection}>
      <Typography color="textSecondary" variant="h6">
        {firstName}{' '}
        <Typography color="textSecondary" variant="caption">
          (@{displayName})
        </Typography>
      </Typography>
    </div>
  );
};

export default UserSection;
