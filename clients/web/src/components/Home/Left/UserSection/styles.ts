import { Theme } from '@mui/material/styles/createTheme';
import { makeStyles } from '@mui/styles';

export default makeStyles<Theme>(() => ({
  containerUserSection: {
    textAlign: 'center',
    marginTop: 7,
  },
}));
