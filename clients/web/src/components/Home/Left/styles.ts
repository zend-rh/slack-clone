import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerLeft: {
    flex: 1,
    maxWidth: 260,
    width: 260,
    backgroundColor: theme.colors.valentino,
  },
  centerSection: {
    maxHeight: 'calc(100vh - 82px)',
    overflowY: 'auto',
  },
}));
