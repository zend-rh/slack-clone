import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles<Theme>(theme => ({
  containerHome: {
    width: '100vw',
    height: '100vh',
    overflow: 'hidden',
    ...theme.containerStyles.flexColumn,
  },
  containerHomeBody: {
    ...theme.containerStyles.flexRow,
    flex: 1,
  },
}));
