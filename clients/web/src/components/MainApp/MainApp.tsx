import { ApolloProvider } from '@apollo/client';
import { ThemeProvider } from '@mui/styles';
import { FC } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { GlobalSnackbar } from '../../common/GlobalSnackbar';
import { apolloClient } from '../../config/apolloClient';
import { Router } from '../../routes';
import myTheme from '../../themes/myTheme';

const MainApp: FC = () => {
  return (
    <ApolloProvider client={apolloClient}>
      <ThemeProvider theme={myTheme}>
        <BrowserRouter>
          <Router />
          <GlobalSnackbar />
        </BrowserRouter>
      </ThemeProvider>
    </ApolloProvider>
  );
};

export default MainApp;
