import { Stack, TextField, Typography } from '@mui/material';
import { FC, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { CustomButton } from '../../common/CustomButton';
import { useCreateSpaceMutation } from '../../generated/graphql';
import { updateCacheAfterAddingSpace } from '../../updateCache/space';
import useStyle from './styles';

const SetupSpace: FC = () => {
  const classes = useStyle();
  const history = useHistory();
  const [step, setStep] = useState(0);
  const [state, setState] = useState({
    spaceName: '',
    themeName: '',
    membersEmail: [''],
  });
  const [createSpace, { loading }] = useCreateSpaceMutation({
    update: updateCacheAfterAddingSpace,
  });
  const handleSubmit = (ignoreStep3?: boolean) => () => {
    if (step === 0 || step === 1) setStep(step + 1);
    else {
      createSpace({
        variables: {
          inputCreateSpace: {
            name: state.spaceName,
            subject: state.themeName,
            usersToEmail: ignoreStep3 ? [] : state.membersEmail,
          },
        },
      }).then(({ data }) => {
        if (data?.createSpace?.id) history.push(`/space/${data?.createSpace?.id}`);
      });
    }
  };
  return (
    <div className={classes.container}>
      <div className={classes.header} />
      <div className={classes.body}>
        <div className={classes.left}>
          <div className={classes.spaceItem}>{state.spaceName}</div>
          {step !== 0 && (
            <div className={classes.canalList}>
              <span>Canaux</span>
              <Stack marginLeft={1} spacing={2} direction="row" marginTop={1}>
                <span>#</span>
                <span>{`${state.themeName}`}</span>
              </Stack>
            </div>
          )}
        </div>
        <div className={classes.right}>
          <Stack direction="column" spacing={3} width="50%" padding={5}>
            {step === 0 && (
              <>
                <Typography color="black" variant="body2">
                  Étape {step + 1} sur 3
                </Typography>
                <Typography color="black" variant="h3" fontWeight={600}>
                  Quel est le nom de votre entreprise ou de votre équipe ?
                </Typography>
                <Typography color="black" variant="body2">
                  Ceci sera le nom de votre espace de travail Slack. Choisissez quelque chose de
                  reconnaissable pour votre équipe.
                </Typography>
                <TextField
                  onChange={evt => {
                    if (evt.target.value.length < 50) {
                      setState({
                        ...state,
                        spaceName: evt.target.value || '',
                      });
                    }
                  }}
                  value={state.spaceName || ''}
                  className={classes.inputText}
                  placeholder="Ex. : Fictions SA Marketing ou Fictions SA Co"
                />
              </>
            )}
            {step === 1 && (
              <>
                <Typography color="black" variant="body2">
                  Étape {step + 1} sur 3
                </Typography>
                <Typography color="black" variant="h3" fontWeight={600}>
                  Sur quoi travaille votre équipe en ce moment ?
                </Typography>
                <Typography color="black" variant="body2">
                  Vous avez le choix : il peut s’agir d’un projet, d’une campagne, d’un événement ou
                  d’un contrat que vous cherchez à signer.
                </Typography>
                <TextField
                  onChange={evt => {
                    if (evt.target.value.length < 50) {
                      setState({
                        ...state,
                        themeName: evt.target.value || '',
                      });
                    }
                  }}
                  value={state.themeName || ''}
                  className={classes.inputText}
                  placeholder="Exemple : budhet trimestre 5, campagne d'automme"
                />
              </>
            )}
            {step === 2 && (
              <>
                <Typography color="black" variant="body2">
                  Étape {step + 1} sur 3
                </Typography>
                <Typography color="black" variant="h3" fontWeight={600}>
                  Quelle est la personne à laquelle vous envoyez le plus d’e-mails au sujet de{' '}
                  {state.themeName} ?
                </Typography>
                <Typography color="black" variant="body2">
                  Ajoutez quelques collègues réguliers à Slack pour découvrir le fonctionnement de
                  l’application.
                </Typography>
                {state.membersEmail.map((user, i) => (
                  <TextField
                    key={i.toString()}
                    onChange={evt => {
                      const clone = state.membersEmail;
                      clone[i] = evt.target.value || '';
                      setState({
                        ...state,
                        membersEmail: clone,
                      });
                    }}
                    value={state.membersEmail[i] || ''}
                    className={classes.inputText}
                    placeholder="Ex: ellis@gmail.com"
                  />
                ))}
                <span
                  onClick={() => {
                    setState({
                      ...state,
                      membersEmail: [...state.membersEmail, ''],
                    });
                  }}
                  className={classes.addUser}
                >
                  Ajouter une autre personne
                </span>
              </>
            )}
            <CustomButton
              onClick={handleSubmit(false)}
              isLoading={loading}
              className={classes.buttonSubmit}
              disabled={step === 0 ? !state.spaceName : step === 1 ? !state.themeName : false}
            >
              {step === 2 ? 'Ajouter des collaborateurs' : 'Suivant'}
            </CustomButton>
            {step === 2 && (
              <CustomButton
                isLoading={loading}
                variant="outlined"
                onClick={handleSubmit(true)}
                disabled={!state.spaceName}
              >
                Ignorez cette etape
              </CustomButton>
            )}
          </Stack>
        </div>
      </div>
    </div>
  );
};

export default SetupSpace;
