import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    backgroundColor: '#390036',
    width: '100%',
    height: 35,
  },
  right: {
    width: '65%',
  },
  left: {
    minWidth: 200,
    width: '15%',
    backgroundColor: theme.colors.grape,
  },
  body: {
    display: 'flex',
    flexDirection: 'row',
    height: '100vh',
  },
  inputText: {
    width: '100%',
    color: theme.colors.black,
  },
  buttonSubmit: {
    marginTop: 110,
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: 20,
    fontWeight: 600,
    backgroundColor: `${theme.colors.grape}!important`,
    [theme.breakpoints.down('xs')]: {
      marginTop: 77,
    },
  },
  spaceItem: {
    padding: 15,
    color: 'white',
    backgroundColor: 'rgb(82,38,83)',
  },
  canalList: {
    color: 'white',
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 15,
    marginTop: 35,
  },
  addUser: {
    color: 'blue',
    '&:hover': {
      textDecoration: 'underline',
      cursor: 'pointer',
    },
  },
}));
