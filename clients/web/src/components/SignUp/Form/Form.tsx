import { Link, Stack, TextField, Typography } from '@mui/material';
import { FC, KeyboardEvent, useState } from 'react';
import { CustomButton } from '../../../common/CustomButton';
import { useLoginMutation, useSignUpMutation } from '../../../generated/graphql';
import authService from '../../../services/authServices';
import ErrorMessage from '../../../services/ErrorMessage';
import useStyle from './styles';

const Form: FC = () => {
  const classes = useStyle();

  const [username, setUsername] = useState<string | null>('');
  const [firstName, setFirstName] = useState<string | null>('');
  const [lastName, setLastName] = useState<string | null>('');
  const [password, setPassword] = useState<string | null>('');
  const [confPassword, setConfPassword] = useState<string | null>('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [errorLog, setErrorLog] = useState('');

  const [signup, { loading: isLoading }] = useSignUpMutation();

  const handleLogin = () => {
    if (password !== confPassword) setErrorLog(`Mot de passe non identique`);
    if (
      !isLoading &&
      username?.trim() &&
      password?.length &&
      confPassword?.length &&
      firstName?.length &&
      lastName?.length
    ) {
      setErrorLog('');
      signup({
        variables: {
          userInput: {
            email: username,
            firstName,
            lastName,
            password,
          },
        },
      })
        .then(resp => {
          if (resp.data?.signUp?.token) {
            authService.setAccessToken(resp.data.signUp.token);
            setIsLoggedIn(true);
          }
        })
        .catch(error => {
          if (error.networkError) {
            setErrorLog('Network error');
          } else if (error.graphQLErrors.length) {
            const firstError = error.graphQLErrors[0].message;
            if (
              [ErrorMessage.PASSWORD_INCORRECT, ErrorMessage.LOGIN_OR_PASSWORD_INCORRECT].includes(
                firstError
              )
            ) {
              setErrorLog('Email ou mot de passe incorrect');
            } else {
              setErrorLog('Internal server error');
            }
          }
        });
    }
  };

  const handleEnterPress = (evt: KeyboardEvent<HTMLDivElement>) => {
    if (evt.key === 'Enter') {
      handleLogin();
    }
  };

  if (isLoggedIn) {
    window.location.href = '/';
    return null;
  }

  return (
    <div className={classes.containerSlackLoginForm} onKeyDown={handleEnterPress}>
      <form onSubmit={handleLogin}>
        <Stack direction="column" spacing={1}>
          <TextField
            onChange={evt => setUsername(evt.target.value || null)}
            value={username || ''}
            error={username === undefined}
            className={classes.inputText}
            placeholder="Email"
          />
          <TextField
            onChange={evt => setLastName(evt.target.value || null)}
            value={lastName || ''}
            error={lastName === undefined}
            className={classes.inputText}
            placeholder="Nom"
          />
          <TextField
            onChange={evt => setFirstName(evt.target.value || null)}
            value={firstName || ''}
            error={firstName === undefined}
            className={classes.inputText}
            placeholder="Prénom"
          />
          <TextField
            onChange={evt => setPassword(evt.target.value || null)}
            value={password || ''}
            error={password === undefined}
            className={classes.inputText}
            type="password"
            placeholder="Mot de passe"
            autoComplete=""
          />
          <TextField
            onChange={evt => setConfPassword(evt.target.value || null)}
            value={confPassword || ''}
            error={confPassword === undefined}
            className={classes.inputText}
            type="password"
            placeholder="Confirmer votre mot de passe"
            autoComplete=""
          />
        </Stack>
        <h5 className={classes.errorMessage}>{errorLog}</h5>
        <CustomButton
          onClick={handleLogin}
          className={classes.buttonSubmit}
          disabled={!password || !username}
          isLoading={isLoading}
        >
          Créer
        </CustomButton>
        <Stack marginTop={2} direction="row" alignItems="center" spacing={1}>
          <Typography color="gray" align="center" variant="caption">
            Déjà un compte ?
          </Typography>
          <Link href="/login">
            <Typography color="blue" align="center" variant="body2">
              Connexion
            </Typography>
          </Link>
        </Stack>
      </form>
    </div>
  );
};

export default Form;
