import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { important } from '../../../utils/materialUtils';

export default makeStyles((theme: Theme) => ({
  containerSlackLoginForm: {
    maxWidth: 400,
    margin: 'auto',
    marginTop: 50,
  },
  inputText: {
    width: '100%',
    color: theme.colors.black,
    marginTop: 15,
    '&:last-child': {
      // [theme.breakpoints.down('xs')]: {
      //   marginTop: 15,
      // },
    },
  },
  buttonSubmit: {
    marginTop: 110,
    width: '100%',
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: 20,
    fontWeight: 600,
    backgroundColor: `${theme.colors.grape}!important`,
    [theme.breakpoints.down('xs')]: {
      marginTop: 77,
    },
  },
  errorMessage: {
    textAlign: 'center',
    margin: 0,
    marginTop: 7,
    fontSize: 14,
    fontWeight: 'normal',
    color: theme.colors.red,
    minHeight: 30,
  },
}));
