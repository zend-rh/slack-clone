import { ApolloClient, createHttpLink, InMemoryCache, split } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';
import authService from '../services/authServices';
import { fieldPaginationFn } from '../utils/paginationUtils';
import config from './config';

const authLink = setContext((_, { headers }) => {
  const token = authService.getAccessToken();
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  };
});

const httpLink = createHttpLink({
  uri: config.servers.graphqlUrl,
});

const wsLink = new WebSocketLink({
  uri: config.servers.graphqlSubscriptionUrl,
  options: {
    reconnect: true,
    connectionParams: {
      authToken: authService.getAccessToken(),
    },
  },
});

const simplePagination = fieldPaginationFn();

const cache: InMemoryCache & {
  readQueryClone: any;
} = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        users: simplePagination,
        videos: simplePagination,
      },
    },
    DiscussionMessages: {
      keyFields: ['userId'],
    },
    ChannelMessages: {
      keyFields: ['channelId'],
    },
  },
}) as any;

cache.readQueryClone = cache.readQuery;

cache.readQuery = (...args) => {
  try {
    return cache.readQueryClone(...args);
  } catch (err) {
    return undefined;
  }
};

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
  },
  wsLink,
  authLink.concat(httpLink)
);

export const apolloClient = new ApolloClient({
  link: splitLink,
  cache,
});
