const config = {
  servers: {
    graphqlUrl: process.env.REACT_APP_GRAPHQL_URL || 'http://localhost:4000/graphql',
    graphqlSubscriptionUrl:
      process.env.REACT_APP_GRAPHQL_SUBSCRIPTION_URL || 'ws://localhost:4000/graphql',
  },
};

export default config;
