/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable no-use-before-define */
import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';

export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Date custom scalar type */
  DateTime: any;
};

export type AuthUser = {
  __typename?: 'AuthUser';
  token: Scalars['String'];
  user: User;
};

export type ChangePasswordInput = {
  email: Scalars['String'];
  newPassword: Scalars['String'];
};

export type Channel = {
  __typename?: 'Channel';
  author?: Maybe<User>;
  authorId?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  isDeleted?: Maybe<Scalars['Boolean']>;
  isDirectChannel?: Maybe<Scalars['Boolean']>;
  isPrivate?: Maybe<Scalars['Boolean']>;
  members?: Maybe<Array<User>>;
  messages?: Maybe<Array<Maybe<Message>>>;
  name: Scalars['String'];
  space: Space;
  spaceId: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type DirectChannel = {
  __typename?: 'DirectChannel';
  channel?: Maybe<Channel>;
  id: Scalars['String'];
  user?: Maybe<User>;
};

export type InputCreateChannel = {
  isPrivate?: Maybe<Scalars['Boolean']>;
  name: Scalars['String'];
  spaceId: Scalars['String'];
};

export type InputCreateInviteUser = {
  id: Scalars['String'];
  usersToEmail?: Maybe<Array<Scalars['String']>>;
};

export type InputCreateMessage = {
  channelId: Scalars['String'];
  text: Scalars['String'];
};

export type InputCreateSpace = {
  name: Scalars['String'];
  subject?: Maybe<Scalars['String']>;
  usersToEmail?: Maybe<Array<Scalars['String']>>;
};

export type InputUpdateChannel = {
  id: Scalars['String'];
  isPrivate?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
};

export type InputUpdateMessage = {
  id: Scalars['String'];
  text?: Maybe<Scalars['String']>;
};

export type Message = {
  __typename?: 'Message';
  author?: Maybe<User>;
  authorId?: Maybe<Scalars['String']>;
  channel: Channel;
  channelId: Scalars['String'];
  createdAt?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  isDeleted?: Maybe<Scalars['Boolean']>;
  isMine?: Maybe<Scalars['Boolean']>;
  text?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  changePassword?: Maybe<User>;
  checkExpiredToken?: Maybe<SuccessReturn>;
  createChannel?: Maybe<Channel>;
  createInviteUser?: Maybe<Space>;
  createMessage?: Maybe<Message>;
  createSpace?: Maybe<Space>;
  deleteChannel?: Maybe<Channel>;
  deleteMessage?: Maybe<Message>;
  forgotPassword?: Maybe<SuccessReturn>;
  login?: Maybe<AuthUser>;
  resetPassword?: Maybe<SuccessReturn>;
  signUp?: Maybe<AuthUser>;
  updateChannel?: Maybe<Channel>;
  updateMessage?: Maybe<Message>;
  updatePassword?: Maybe<User>;
  updateProfile?: Maybe<User>;
};

export type MutationChangePasswordArgs = {
  input: ChangePasswordInput;
};

export type MutationCheckExpiredTokenArgs = {
  email: Scalars['String'];
  token: Scalars['String'];
};

export type MutationCreateChannelArgs = {
  inputCreateChannel: InputCreateChannel;
};

export type MutationCreateInviteUserArgs = {
  inputCreateInviteUser: InputCreateInviteUser;
};

export type MutationCreateMessageArgs = {
  inputCreateMessage: InputCreateMessage;
};

export type MutationCreateSpaceArgs = {
  inputCreateSpace: InputCreateSpace;
};

export type MutationDeleteChannelArgs = {
  id: Scalars['String'];
};

export type MutationDeleteMessageArgs = {
  id: Scalars['String'];
};

export type MutationForgotPasswordArgs = {
  email: Scalars['String'];
};

export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type MutationResetPasswordArgs = {
  input: ResetPasswordInput;
};

export type MutationSignUpArgs = {
  userInput: UserCreateInput;
};

export type MutationUpdateChannelArgs = {
  inputUpdateChannel: InputUpdateChannel;
};

export type MutationUpdateMessageArgs = {
  inputUpdateMessage: InputUpdateMessage;
};

export type MutationUpdatePasswordArgs = {
  id: Scalars['String'];
  newPassword: Scalars['String'];
  oldPassword: Scalars['String'];
};

export type MutationUpdateProfileArgs = {
  id: Scalars['String'];
  input: UserProfileInput;
};

export type Query = {
  __typename?: 'Query';
  channel?: Maybe<Channel>;
  me?: Maybe<User>;
  space?: Maybe<Space>;
};

export type QueryChannelArgs = {
  id: Scalars['String'];
};

export type QuerySpaceArgs = {
  id: Scalars['String'];
};

export type ResetPasswordInput = {
  email: Scalars['String'];
  newPassword: Scalars['String'];
  token: Scalars['String'];
};

export type Space = {
  __typename?: 'Space';
  channels?: Maybe<Array<Maybe<Channel>>>;
  createdAt?: Maybe<Scalars['DateTime']>;
  directChannels?: Maybe<Array<Maybe<DirectChannel>>>;
  id: Scalars['ID'];
  isDeleted?: Maybe<Scalars['Boolean']>;
  members?: Maybe<Array<User>>;
  name: Scalars['String'];
  notMembers?: Maybe<Array<User>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type Subscription = {
  __typename?: 'Subscription';
  deleteChannel?: Maybe<Channel>;
  deleteMessage?: Maybe<Message>;
  editChannel?: Maybe<Channel>;
  editMessage?: Maybe<Message>;
  editUser?: Maybe<User>;
  newChannel?: Maybe<Channel>;
  newMessage?: Maybe<Message>;
  updateUser?: Maybe<User>;
};

export type SubscriptionDeleteChannelArgs = {
  spaceId: Scalars['String'];
};

export type SubscriptionDeleteMessageArgs = {
  channelId: Scalars['String'];
};

export type SubscriptionEditChannelArgs = {
  spaceId: Scalars['String'];
};

export type SubscriptionEditMessageArgs = {
  channelId: Scalars['String'];
};

export type SubscriptionEditUserArgs = {
  userId: Scalars['String'];
};

export type SubscriptionNewChannelArgs = {
  spaceId: Scalars['String'];
};

export type SubscriptionNewMessageArgs = {
  channelId: Scalars['String'];
};

export type SuccessReturn = {
  __typename?: 'SuccessReturn';
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type User = {
  __typename?: 'User';
  avatar?: Maybe<Scalars['String']>;
  createChannels: Array<Channel>;
  createdAt?: Maybe<Scalars['DateTime']>;
  displayName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  isBanned?: Maybe<Scalars['Boolean']>;
  isMe?: Maybe<Scalars['Boolean']>;
  isOnline?: Maybe<Scalars['Boolean']>;
  isRemoved?: Maybe<Scalars['Boolean']>;
  joinChannels: Array<Channel>;
  joinSpaces: Array<Space>;
  lastConnectedTime?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type UserCreateInput = {
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  password: Scalars['String'];
};

export type UserProfileInput = {
  displayName: Scalars['String'];
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
};

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;

export type LoginMutation = {
  __typename?: 'Mutation';
  login?:
    | { __typename?: 'AuthUser'; token: string; user: { __typename?: 'User'; id: string } }
    | null
    | undefined;
};

export type SignUpMutationVariables = Exact<{
  userInput: UserCreateInput;
}>;

export type SignUpMutation = {
  __typename?: 'Mutation';
  signUp?:
    | { __typename?: 'AuthUser'; token: string; user: { __typename?: 'User'; id: string } }
    | null
    | undefined;
};

export type ChannelInfosFragment = {
  __typename?: 'Channel';
  id: string;
  name: string;
  authorId?: string | null | undefined;
  spaceId: string;
  createdAt?: any | null | undefined;
  updatedAt?: any | null | undefined;
  isDeleted?: boolean | null | undefined;
  isPrivate?: boolean | null | undefined;
};

export type CreateChannelMutationVariables = Exact<{
  inputCreateChannel: InputCreateChannel;
}>;

export type CreateChannelMutation = {
  __typename?: 'Mutation';
  createChannel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
      }
    | null
    | undefined;
};

export type UpdateChannelMutationVariables = Exact<{
  inputUpdateChannel: InputUpdateChannel;
}>;

export type UpdateChannelMutation = {
  __typename?: 'Mutation';
  updateChannel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
      }
    | null
    | undefined;
};

export type DeleteChannelMutationVariables = Exact<{
  id: Scalars['String'];
}>;

export type DeleteChannelMutation = {
  __typename?: 'Mutation';
  deleteChannel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
      }
    | null
    | undefined;
};

export type GetChannelMessagesQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type GetChannelMessagesQuery = {
  __typename?: 'Query';
  channel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
        messages?:
          | Array<
              | {
                  __typename?: 'Message';
                  id: string;
                  text?: string | null | undefined;
                  createdAt?: any | null | undefined;
                  updatedAt?: any | null | undefined;
                  isMine?: boolean | null | undefined;
                  isDeleted?: boolean | null | undefined;
                  authorId?: string | null | undefined;
                  channelId: string;
                  author?:
                    | {
                        __typename?: 'User';
                        id: string;
                        email?: string | null | undefined;
                        avatar?: string | null | undefined;
                        firstName?: string | null | undefined;
                        lastName?: string | null | undefined;
                        displayName?: string | null | undefined;
                        isOnline?: boolean | null | undefined;
                        lastConnectedTime?: string | null | undefined;
                        isMe?: boolean | null | undefined;
                        createdAt?: any | null | undefined;
                        updatedAt?: any | null | undefined;
                      }
                    | null
                    | undefined;
                }
              | null
              | undefined
            >
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type GetChannelMembersQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type GetChannelMembersQuery = {
  __typename?: 'Query';
  channel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
        members?:
          | Array<{
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }>
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type SubscribeToNewChannelSubscriptionVariables = Exact<{
  spaceId: Scalars['String'];
}>;

export type SubscribeToNewChannelSubscription = {
  __typename?: 'Subscription';
  newChannel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
      }
    | null
    | undefined;
};

export type SubscribeToEditChannelSubscriptionVariables = Exact<{
  spaceId: Scalars['String'];
}>;

export type SubscribeToEditChannelSubscription = {
  __typename?: 'Subscription';
  editChannel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
      }
    | null
    | undefined;
};

export type SubscribeToDeleteChannelSubscriptionVariables = Exact<{
  spaceId: Scalars['String'];
}>;

export type SubscribeToDeleteChannelSubscription = {
  __typename?: 'Subscription';
  deleteChannel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        authorId?: string | null | undefined;
        spaceId: string;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isDeleted?: boolean | null | undefined;
        isPrivate?: boolean | null | undefined;
      }
    | null
    | undefined;
};

export type MessageInfosFragment = {
  __typename?: 'Message';
  id: string;
  text?: string | null | undefined;
  createdAt?: any | null | undefined;
  updatedAt?: any | null | undefined;
  isMine?: boolean | null | undefined;
  isDeleted?: boolean | null | undefined;
  authorId?: string | null | undefined;
  channelId: string;
  author?:
    | {
        __typename?: 'User';
        id: string;
        email?: string | null | undefined;
        avatar?: string | null | undefined;
        firstName?: string | null | undefined;
        lastName?: string | null | undefined;
        displayName?: string | null | undefined;
        isOnline?: boolean | null | undefined;
        lastConnectedTime?: string | null | undefined;
        isMe?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
      }
    | null
    | undefined;
};

export type CreateMessageMutationVariables = Exact<{
  inputCreateMessage: InputCreateMessage;
}>;

export type CreateMessageMutation = {
  __typename?: 'Mutation';
  createMessage?:
    | {
        __typename?: 'Message';
        id: string;
        text?: string | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isMine?: boolean | null | undefined;
        isDeleted?: boolean | null | undefined;
        authorId?: string | null | undefined;
        channelId: string;
        author?:
          | {
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type UpdateMessageMutationVariables = Exact<{
  inputUpdateMessage: InputUpdateMessage;
}>;

export type UpdateMessageMutation = {
  __typename?: 'Mutation';
  updateMessage?:
    | {
        __typename?: 'Message';
        id: string;
        text?: string | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isMine?: boolean | null | undefined;
        isDeleted?: boolean | null | undefined;
        authorId?: string | null | undefined;
        channelId: string;
        author?:
          | {
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type DeleteMessageMutationVariables = Exact<{
  id: Scalars['String'];
}>;

export type DeleteMessageMutation = {
  __typename?: 'Mutation';
  deleteMessage?:
    | {
        __typename?: 'Message';
        id: string;
        text?: string | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isMine?: boolean | null | undefined;
        isDeleted?: boolean | null | undefined;
        authorId?: string | null | undefined;
        channelId: string;
        author?:
          | {
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type SubscribeToNewMessageSubscriptionVariables = Exact<{
  channelId: Scalars['String'];
}>;

export type SubscribeToNewMessageSubscription = {
  __typename?: 'Subscription';
  newMessage?:
    | {
        __typename?: 'Message';
        id: string;
        text?: string | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isMine?: boolean | null | undefined;
        isDeleted?: boolean | null | undefined;
        authorId?: string | null | undefined;
        channelId: string;
        author?:
          | {
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type SubscribeToEditMessageSubscriptionVariables = Exact<{
  channelId: Scalars['String'];
}>;

export type SubscribeToEditMessageSubscription = {
  __typename?: 'Subscription';
  editMessage?:
    | {
        __typename?: 'Message';
        id: string;
        text?: string | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isMine?: boolean | null | undefined;
        isDeleted?: boolean | null | undefined;
        authorId?: string | null | undefined;
        channelId: string;
        author?:
          | {
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type SubscribeToDeleteMessageSubscriptionVariables = Exact<{
  channelId: Scalars['String'];
}>;

export type SubscribeToDeleteMessageSubscription = {
  __typename?: 'Subscription';
  deleteMessage?:
    | {
        __typename?: 'Message';
        id: string;
        text?: string | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        isMine?: boolean | null | undefined;
        isDeleted?: boolean | null | undefined;
        authorId?: string | null | undefined;
        channelId: string;
        author?:
          | {
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type SpaceBasicInfosFragment = {
  __typename?: 'Space';
  id: string;
  name: string;
  isDeleted?: boolean | null | undefined;
  createdAt?: any | null | undefined;
  updatedAt?: any | null | undefined;
};

export type CreateSpaceMutationVariables = Exact<{
  inputCreateSpace: InputCreateSpace;
}>;

export type CreateSpaceMutation = {
  __typename?: 'Mutation';
  createSpace?:
    | {
        __typename?: 'Space';
        id: string;
        name: string;
        isDeleted?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
      }
    | null
    | undefined;
};

export type InviteUserToSpaceMutationVariables = Exact<{
  inputCreateInviteUser: InputCreateInviteUser;
}>;

export type InviteUserToSpaceMutation = {
  __typename?: 'Mutation';
  createInviteUser?:
    | {
        __typename?: 'Space';
        id: string;
        name: string;
        isDeleted?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        members?:
          | Array<{
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }>
          | null
          | undefined;
        notMembers?:
          | Array<{
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }>
          | null
          | undefined;
        directChannels?:
          | Array<
              | {
                  __typename?: 'DirectChannel';
                  id: string;
                  user?:
                    | {
                        __typename?: 'User';
                        id: string;
                        email?: string | null | undefined;
                        avatar?: string | null | undefined;
                        firstName?: string | null | undefined;
                        lastName?: string | null | undefined;
                        displayName?: string | null | undefined;
                        isOnline?: boolean | null | undefined;
                        lastConnectedTime?: string | null | undefined;
                        isMe?: boolean | null | undefined;
                        createdAt?: any | null | undefined;
                        updatedAt?: any | null | undefined;
                      }
                    | null
                    | undefined;
                  channel?:
                    | {
                        __typename?: 'Channel';
                        id: string;
                        name: string;
                        authorId?: string | null | undefined;
                        spaceId: string;
                        createdAt?: any | null | undefined;
                        updatedAt?: any | null | undefined;
                        isDeleted?: boolean | null | undefined;
                        isPrivate?: boolean | null | undefined;
                      }
                    | null
                    | undefined;
                }
              | null
              | undefined
            >
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type GetMySpacesQueryVariables = Exact<{ [key: string]: never }>;

export type GetMySpacesQuery = {
  __typename?: 'Query';
  me?:
    | {
        __typename?: 'User';
        id: string;
        joinSpaces: Array<{
          __typename?: 'Space';
          id: string;
          name: string;
          isDeleted?: boolean | null | undefined;
          createdAt?: any | null | undefined;
          updatedAt?: any | null | undefined;
          members?: Array<{ __typename?: 'User'; id: string }> | null | undefined;
        }>;
      }
    | null
    | undefined;
};

export type GetSpaceChannelsQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type GetSpaceChannelsQuery = {
  __typename?: 'Query';
  space?:
    | {
        __typename?: 'Space';
        id: string;
        name: string;
        isDeleted?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        members?:
          | Array<{
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }>
          | null
          | undefined;
        channels?:
          | Array<
              | {
                  __typename?: 'Channel';
                  id: string;
                  name: string;
                  authorId?: string | null | undefined;
                  spaceId: string;
                  createdAt?: any | null | undefined;
                  updatedAt?: any | null | undefined;
                  isDeleted?: boolean | null | undefined;
                  isPrivate?: boolean | null | undefined;
                }
              | null
              | undefined
            >
          | null
          | undefined;
        directChannels?:
          | Array<
              | {
                  __typename?: 'DirectChannel';
                  id: string;
                  user?:
                    | {
                        __typename?: 'User';
                        id: string;
                        email?: string | null | undefined;
                        avatar?: string | null | undefined;
                        firstName?: string | null | undefined;
                        lastName?: string | null | undefined;
                        displayName?: string | null | undefined;
                        isOnline?: boolean | null | undefined;
                        lastConnectedTime?: string | null | undefined;
                        isMe?: boolean | null | undefined;
                        createdAt?: any | null | undefined;
                        updatedAt?: any | null | undefined;
                      }
                    | null
                    | undefined;
                  channel?:
                    | {
                        __typename?: 'Channel';
                        id: string;
                        name: string;
                        authorId?: string | null | undefined;
                        spaceId: string;
                        createdAt?: any | null | undefined;
                        updatedAt?: any | null | undefined;
                        isDeleted?: boolean | null | undefined;
                        isPrivate?: boolean | null | undefined;
                      }
                    | null
                    | undefined;
                }
              | null
              | undefined
            >
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type GetSpaceMembersQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type GetSpaceMembersQuery = {
  __typename?: 'Query';
  space?:
    | {
        __typename?: 'Space';
        id: string;
        name: string;
        isDeleted?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
        members?:
          | Array<{
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }>
          | null
          | undefined;
        notMembers?:
          | Array<{
              __typename?: 'User';
              id: string;
              email?: string | null | undefined;
              avatar?: string | null | undefined;
              firstName?: string | null | undefined;
              lastName?: string | null | undefined;
              displayName?: string | null | undefined;
              isOnline?: boolean | null | undefined;
              lastConnectedTime?: string | null | undefined;
              isMe?: boolean | null | undefined;
              createdAt?: any | null | undefined;
              updatedAt?: any | null | undefined;
            }>
          | null
          | undefined;
      }
    | null
    | undefined;
};

export type GetSpaceInfoQueryVariables = Exact<{
  id: Scalars['String'];
}>;

export type GetSpaceInfoQuery = {
  __typename?: 'Query';
  space?:
    | {
        __typename?: 'Space';
        id: string;
        name: string;
        isDeleted?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
      }
    | null
    | undefined;
};

export type UserBasicInfosFragment = {
  __typename?: 'User';
  id: string;
  email?: string | null | undefined;
  avatar?: string | null | undefined;
  firstName?: string | null | undefined;
  lastName?: string | null | undefined;
  displayName?: string | null | undefined;
  isOnline?: boolean | null | undefined;
  lastConnectedTime?: string | null | undefined;
  isMe?: boolean | null | undefined;
  createdAt?: any | null | undefined;
  updatedAt?: any | null | undefined;
};

export type UpdateUserMutationVariables = Exact<{
  id: Scalars['String'];
  input: UserProfileInput;
}>;

export type UpdateUserMutation = {
  __typename?: 'Mutation';
  updateProfile?:
    | {
        __typename?: 'User';
        id: string;
        email?: string | null | undefined;
        avatar?: string | null | undefined;
        firstName?: string | null | undefined;
        lastName?: string | null | undefined;
        displayName?: string | null | undefined;
        isOnline?: boolean | null | undefined;
        lastConnectedTime?: string | null | undefined;
        isMe?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
      }
    | null
    | undefined;
};

export type MeQueryVariables = Exact<{ [key: string]: never }>;

export type MeQuery = {
  __typename?: 'Query';
  me?:
    | {
        __typename?: 'User';
        id: string;
        email?: string | null | undefined;
        avatar?: string | null | undefined;
        firstName?: string | null | undefined;
        lastName?: string | null | undefined;
        displayName?: string | null | undefined;
        isOnline?: boolean | null | undefined;
        lastConnectedTime?: string | null | undefined;
        isMe?: boolean | null | undefined;
        createdAt?: any | null | undefined;
        updatedAt?: any | null | undefined;
      }
    | null
    | undefined;
};

export type SubscribeToUserStatusSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToUserStatusSubscription = {
  __typename?: 'Subscription';
  updateUser?:
    | {
        __typename?: 'User';
        id: string;
        isOnline?: boolean | null | undefined;
        lastConnectedTime?: string | null | undefined;
      }
    | null
    | undefined;
};

export const ChannelInfosFragmentDoc = gql`
  fragment ChannelInfos on Channel {
    id
    name
    authorId
    spaceId
    createdAt
    updatedAt
    isDeleted
    isPrivate
  }
`;
export const UserBasicInfosFragmentDoc = gql`
  fragment UserBasicInfos on User {
    id
    email
    avatar
    firstName
    lastName
    displayName
    isOnline
    lastConnectedTime
    isMe
    createdAt
    updatedAt
  }
`;
export const MessageInfosFragmentDoc = gql`
  fragment MessageInfos on Message {
    id
    text
    createdAt
    updatedAt
    isMine
    isDeleted
    authorId
    channelId
    author {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;
export const SpaceBasicInfosFragmentDoc = gql`
  fragment SpaceBasicInfos on Space {
    id
    name
    isDeleted
    createdAt
    updatedAt
  }
`;
export const LoginDocument = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        id
      }
    }
  }
`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(
  baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<
  LoginMutation,
  LoginMutationVariables
>;
export const SignUpDocument = gql`
  mutation SignUp($userInput: UserCreateInput!) {
    signUp(userInput: $userInput) {
      token
      user {
        id
      }
    }
  }
`;
export type SignUpMutationFn = Apollo.MutationFunction<SignUpMutation, SignUpMutationVariables>;

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      userInput: // value for 'userInput'
 *   },
 * });
 */
export function useSignUpMutation(
  baseOptions?: Apollo.MutationHookOptions<SignUpMutation, SignUpMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, options);
}
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export type SignUpMutationResult = Apollo.MutationResult<SignUpMutation>;
export type SignUpMutationOptions = Apollo.BaseMutationOptions<
  SignUpMutation,
  SignUpMutationVariables
>;
export const CreateChannelDocument = gql`
  mutation CreateChannel($inputCreateChannel: InputCreateChannel!) {
    createChannel(inputCreateChannel: $inputCreateChannel) {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;
export type CreateChannelMutationFn = Apollo.MutationFunction<
  CreateChannelMutation,
  CreateChannelMutationVariables
>;

/**
 * __useCreateChannelMutation__
 *
 * To run a mutation, you first call `useCreateChannelMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateChannelMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createChannelMutation, { data, loading, error }] = useCreateChannelMutation({
 *   variables: {
 *      inputCreateChannel: // value for 'inputCreateChannel'
 *   },
 * });
 */
export function useCreateChannelMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateChannelMutation, CreateChannelMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateChannelMutation, CreateChannelMutationVariables>(
    CreateChannelDocument,
    options
  );
}
export type CreateChannelMutationHookResult = ReturnType<typeof useCreateChannelMutation>;
export type CreateChannelMutationResult = Apollo.MutationResult<CreateChannelMutation>;
export type CreateChannelMutationOptions = Apollo.BaseMutationOptions<
  CreateChannelMutation,
  CreateChannelMutationVariables
>;
export const UpdateChannelDocument = gql`
  mutation UpdateChannel($inputUpdateChannel: InputUpdateChannel!) {
    updateChannel(inputUpdateChannel: $inputUpdateChannel) {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;
export type UpdateChannelMutationFn = Apollo.MutationFunction<
  UpdateChannelMutation,
  UpdateChannelMutationVariables
>;

/**
 * __useUpdateChannelMutation__
 *
 * To run a mutation, you first call `useUpdateChannelMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateChannelMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateChannelMutation, { data, loading, error }] = useUpdateChannelMutation({
 *   variables: {
 *      inputUpdateChannel: // value for 'inputUpdateChannel'
 *   },
 * });
 */
export function useUpdateChannelMutation(
  baseOptions?: Apollo.MutationHookOptions<UpdateChannelMutation, UpdateChannelMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateChannelMutation, UpdateChannelMutationVariables>(
    UpdateChannelDocument,
    options
  );
}
export type UpdateChannelMutationHookResult = ReturnType<typeof useUpdateChannelMutation>;
export type UpdateChannelMutationResult = Apollo.MutationResult<UpdateChannelMutation>;
export type UpdateChannelMutationOptions = Apollo.BaseMutationOptions<
  UpdateChannelMutation,
  UpdateChannelMutationVariables
>;
export const DeleteChannelDocument = gql`
  mutation DeleteChannel($id: String!) {
    deleteChannel(id: $id) {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;
export type DeleteChannelMutationFn = Apollo.MutationFunction<
  DeleteChannelMutation,
  DeleteChannelMutationVariables
>;

/**
 * __useDeleteChannelMutation__
 *
 * To run a mutation, you first call `useDeleteChannelMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteChannelMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteChannelMutation, { data, loading, error }] = useDeleteChannelMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteChannelMutation(
  baseOptions?: Apollo.MutationHookOptions<DeleteChannelMutation, DeleteChannelMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<DeleteChannelMutation, DeleteChannelMutationVariables>(
    DeleteChannelDocument,
    options
  );
}
export type DeleteChannelMutationHookResult = ReturnType<typeof useDeleteChannelMutation>;
export type DeleteChannelMutationResult = Apollo.MutationResult<DeleteChannelMutation>;
export type DeleteChannelMutationOptions = Apollo.BaseMutationOptions<
  DeleteChannelMutation,
  DeleteChannelMutationVariables
>;
export const GetChannelMessagesDocument = gql`
  query GetChannelMessages($id: String!) {
    channel(id: $id) {
      ...ChannelInfos
      messages {
        ...MessageInfos
        author {
          ...UserBasicInfos
        }
      }
    }
  }
  ${ChannelInfosFragmentDoc}
  ${MessageInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useGetChannelMessagesQuery__
 *
 * To run a query within a React component, call `useGetChannelMessagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetChannelMessagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetChannelMessagesQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetChannelMessagesQuery(
  baseOptions: Apollo.QueryHookOptions<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>(
    GetChannelMessagesDocument,
    options
  );
}
export function useGetChannelMessagesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetChannelMessagesQuery,
    GetChannelMessagesQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>(
    GetChannelMessagesDocument,
    options
  );
}
export type GetChannelMessagesQueryHookResult = ReturnType<typeof useGetChannelMessagesQuery>;
export type GetChannelMessagesLazyQueryHookResult = ReturnType<
  typeof useGetChannelMessagesLazyQuery
>;
export type GetChannelMessagesQueryResult = Apollo.QueryResult<
  GetChannelMessagesQuery,
  GetChannelMessagesQueryVariables
>;
export const GetChannelMembersDocument = gql`
  query GetChannelMembers($id: String!) {
    channel(id: $id) {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${ChannelInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useGetChannelMembersQuery__
 *
 * To run a query within a React component, call `useGetChannelMembersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetChannelMembersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetChannelMembersQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetChannelMembersQuery(
  baseOptions: Apollo.QueryHookOptions<GetChannelMembersQuery, GetChannelMembersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetChannelMembersQuery, GetChannelMembersQueryVariables>(
    GetChannelMembersDocument,
    options
  );
}
export function useGetChannelMembersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetChannelMembersQuery, GetChannelMembersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetChannelMembersQuery, GetChannelMembersQueryVariables>(
    GetChannelMembersDocument,
    options
  );
}
export type GetChannelMembersQueryHookResult = ReturnType<typeof useGetChannelMembersQuery>;
export type GetChannelMembersLazyQueryHookResult = ReturnType<typeof useGetChannelMembersLazyQuery>;
export type GetChannelMembersQueryResult = Apollo.QueryResult<
  GetChannelMembersQuery,
  GetChannelMembersQueryVariables
>;
export const SubscribeToNewChannelDocument = gql`
  subscription SubscribeToNewChannel($spaceId: String!) {
    newChannel(spaceId: $spaceId) {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;

/**
 * __useSubscribeToNewChannelSubscription__
 *
 * To run a query within a React component, call `useSubscribeToNewChannelSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToNewChannelSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToNewChannelSubscription({
 *   variables: {
 *      spaceId: // value for 'spaceId'
 *   },
 * });
 */
export function useSubscribeToNewChannelSubscription(
  baseOptions: Apollo.SubscriptionHookOptions<
    SubscribeToNewChannelSubscription,
    SubscribeToNewChannelSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToNewChannelSubscription,
    SubscribeToNewChannelSubscriptionVariables
  >(SubscribeToNewChannelDocument, options);
}
export type SubscribeToNewChannelSubscriptionHookResult = ReturnType<
  typeof useSubscribeToNewChannelSubscription
>;
export type SubscribeToNewChannelSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToNewChannelSubscription>;
export const SubscribeToEditChannelDocument = gql`
  subscription SubscribeToEditChannel($spaceId: String!) {
    editChannel(spaceId: $spaceId) {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;

/**
 * __useSubscribeToEditChannelSubscription__
 *
 * To run a query within a React component, call `useSubscribeToEditChannelSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToEditChannelSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToEditChannelSubscription({
 *   variables: {
 *      spaceId: // value for 'spaceId'
 *   },
 * });
 */
export function useSubscribeToEditChannelSubscription(
  baseOptions: Apollo.SubscriptionHookOptions<
    SubscribeToEditChannelSubscription,
    SubscribeToEditChannelSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToEditChannelSubscription,
    SubscribeToEditChannelSubscriptionVariables
  >(SubscribeToEditChannelDocument, options);
}
export type SubscribeToEditChannelSubscriptionHookResult = ReturnType<
  typeof useSubscribeToEditChannelSubscription
>;
export type SubscribeToEditChannelSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToEditChannelSubscription>;
export const SubscribeToDeleteChannelDocument = gql`
  subscription SubscribeToDeleteChannel($spaceId: String!) {
    deleteChannel(spaceId: $spaceId) {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;

/**
 * __useSubscribeToDeleteChannelSubscription__
 *
 * To run a query within a React component, call `useSubscribeToDeleteChannelSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToDeleteChannelSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToDeleteChannelSubscription({
 *   variables: {
 *      spaceId: // value for 'spaceId'
 *   },
 * });
 */
export function useSubscribeToDeleteChannelSubscription(
  baseOptions: Apollo.SubscriptionHookOptions<
    SubscribeToDeleteChannelSubscription,
    SubscribeToDeleteChannelSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToDeleteChannelSubscription,
    SubscribeToDeleteChannelSubscriptionVariables
  >(SubscribeToDeleteChannelDocument, options);
}
export type SubscribeToDeleteChannelSubscriptionHookResult = ReturnType<
  typeof useSubscribeToDeleteChannelSubscription
>;
export type SubscribeToDeleteChannelSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToDeleteChannelSubscription>;
export const CreateMessageDocument = gql`
  mutation CreateMessage($inputCreateMessage: InputCreateMessage!) {
    createMessage(inputCreateMessage: $inputCreateMessage) {
      ...MessageInfos
      author {
        ...UserBasicInfos
      }
    }
  }
  ${MessageInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;
export type CreateMessageMutationFn = Apollo.MutationFunction<
  CreateMessageMutation,
  CreateMessageMutationVariables
>;

/**
 * __useCreateMessageMutation__
 *
 * To run a mutation, you first call `useCreateMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createMessageMutation, { data, loading, error }] = useCreateMessageMutation({
 *   variables: {
 *      inputCreateMessage: // value for 'inputCreateMessage'
 *   },
 * });
 */
export function useCreateMessageMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateMessageMutation, CreateMessageMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateMessageMutation, CreateMessageMutationVariables>(
    CreateMessageDocument,
    options
  );
}
export type CreateMessageMutationHookResult = ReturnType<typeof useCreateMessageMutation>;
export type CreateMessageMutationResult = Apollo.MutationResult<CreateMessageMutation>;
export type CreateMessageMutationOptions = Apollo.BaseMutationOptions<
  CreateMessageMutation,
  CreateMessageMutationVariables
>;
export const UpdateMessageDocument = gql`
  mutation UpdateMessage($inputUpdateMessage: InputUpdateMessage!) {
    updateMessage(inputUpdateMessage: $inputUpdateMessage) {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;
export type UpdateMessageMutationFn = Apollo.MutationFunction<
  UpdateMessageMutation,
  UpdateMessageMutationVariables
>;

/**
 * __useUpdateMessageMutation__
 *
 * To run a mutation, you first call `useUpdateMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateMessageMutation, { data, loading, error }] = useUpdateMessageMutation({
 *   variables: {
 *      inputUpdateMessage: // value for 'inputUpdateMessage'
 *   },
 * });
 */
export function useUpdateMessageMutation(
  baseOptions?: Apollo.MutationHookOptions<UpdateMessageMutation, UpdateMessageMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateMessageMutation, UpdateMessageMutationVariables>(
    UpdateMessageDocument,
    options
  );
}
export type UpdateMessageMutationHookResult = ReturnType<typeof useUpdateMessageMutation>;
export type UpdateMessageMutationResult = Apollo.MutationResult<UpdateMessageMutation>;
export type UpdateMessageMutationOptions = Apollo.BaseMutationOptions<
  UpdateMessageMutation,
  UpdateMessageMutationVariables
>;
export const DeleteMessageDocument = gql`
  mutation DeleteMessage($id: String!) {
    deleteMessage(id: $id) {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;
export type DeleteMessageMutationFn = Apollo.MutationFunction<
  DeleteMessageMutation,
  DeleteMessageMutationVariables
>;

/**
 * __useDeleteMessageMutation__
 *
 * To run a mutation, you first call `useDeleteMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMessageMutation, { data, loading, error }] = useDeleteMessageMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMessageMutation(
  baseOptions?: Apollo.MutationHookOptions<DeleteMessageMutation, DeleteMessageMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<DeleteMessageMutation, DeleteMessageMutationVariables>(
    DeleteMessageDocument,
    options
  );
}
export type DeleteMessageMutationHookResult = ReturnType<typeof useDeleteMessageMutation>;
export type DeleteMessageMutationResult = Apollo.MutationResult<DeleteMessageMutation>;
export type DeleteMessageMutationOptions = Apollo.BaseMutationOptions<
  DeleteMessageMutation,
  DeleteMessageMutationVariables
>;
export const SubscribeToNewMessageDocument = gql`
  subscription SubscribeToNewMessage($channelId: String!) {
    newMessage(channelId: $channelId) {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;

/**
 * __useSubscribeToNewMessageSubscription__
 *
 * To run a query within a React component, call `useSubscribeToNewMessageSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToNewMessageSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToNewMessageSubscription({
 *   variables: {
 *      channelId: // value for 'channelId'
 *   },
 * });
 */
export function useSubscribeToNewMessageSubscription(
  baseOptions: Apollo.SubscriptionHookOptions<
    SubscribeToNewMessageSubscription,
    SubscribeToNewMessageSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToNewMessageSubscription,
    SubscribeToNewMessageSubscriptionVariables
  >(SubscribeToNewMessageDocument, options);
}
export type SubscribeToNewMessageSubscriptionHookResult = ReturnType<
  typeof useSubscribeToNewMessageSubscription
>;
export type SubscribeToNewMessageSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToNewMessageSubscription>;
export const SubscribeToEditMessageDocument = gql`
  subscription SubscribeToEditMessage($channelId: String!) {
    editMessage(channelId: $channelId) {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;

/**
 * __useSubscribeToEditMessageSubscription__
 *
 * To run a query within a React component, call `useSubscribeToEditMessageSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToEditMessageSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToEditMessageSubscription({
 *   variables: {
 *      channelId: // value for 'channelId'
 *   },
 * });
 */
export function useSubscribeToEditMessageSubscription(
  baseOptions: Apollo.SubscriptionHookOptions<
    SubscribeToEditMessageSubscription,
    SubscribeToEditMessageSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToEditMessageSubscription,
    SubscribeToEditMessageSubscriptionVariables
  >(SubscribeToEditMessageDocument, options);
}
export type SubscribeToEditMessageSubscriptionHookResult = ReturnType<
  typeof useSubscribeToEditMessageSubscription
>;
export type SubscribeToEditMessageSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToEditMessageSubscription>;
export const SubscribeToDeleteMessageDocument = gql`
  subscription SubscribeToDeleteMessage($channelId: String!) {
    deleteMessage(channelId: $channelId) {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;

/**
 * __useSubscribeToDeleteMessageSubscription__
 *
 * To run a query within a React component, call `useSubscribeToDeleteMessageSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToDeleteMessageSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToDeleteMessageSubscription({
 *   variables: {
 *      channelId: // value for 'channelId'
 *   },
 * });
 */
export function useSubscribeToDeleteMessageSubscription(
  baseOptions: Apollo.SubscriptionHookOptions<
    SubscribeToDeleteMessageSubscription,
    SubscribeToDeleteMessageSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToDeleteMessageSubscription,
    SubscribeToDeleteMessageSubscriptionVariables
  >(SubscribeToDeleteMessageDocument, options);
}
export type SubscribeToDeleteMessageSubscriptionHookResult = ReturnType<
  typeof useSubscribeToDeleteMessageSubscription
>;
export type SubscribeToDeleteMessageSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToDeleteMessageSubscription>;
export const CreateSpaceDocument = gql`
  mutation CreateSpace($inputCreateSpace: InputCreateSpace!) {
    createSpace(inputCreateSpace: $inputCreateSpace) {
      ...SpaceBasicInfos
    }
  }
  ${SpaceBasicInfosFragmentDoc}
`;
export type CreateSpaceMutationFn = Apollo.MutationFunction<
  CreateSpaceMutation,
  CreateSpaceMutationVariables
>;

/**
 * __useCreateSpaceMutation__
 *
 * To run a mutation, you first call `useCreateSpaceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSpaceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSpaceMutation, { data, loading, error }] = useCreateSpaceMutation({
 *   variables: {
 *      inputCreateSpace: // value for 'inputCreateSpace'
 *   },
 * });
 */
export function useCreateSpaceMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateSpaceMutation, CreateSpaceMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateSpaceMutation, CreateSpaceMutationVariables>(
    CreateSpaceDocument,
    options
  );
}
export type CreateSpaceMutationHookResult = ReturnType<typeof useCreateSpaceMutation>;
export type CreateSpaceMutationResult = Apollo.MutationResult<CreateSpaceMutation>;
export type CreateSpaceMutationOptions = Apollo.BaseMutationOptions<
  CreateSpaceMutation,
  CreateSpaceMutationVariables
>;
export const InviteUserToSpaceDocument = gql`
  mutation InviteUserToSpace($inputCreateInviteUser: InputCreateInviteUser!) {
    createInviteUser(inputCreateInviteUser: $inputCreateInviteUser) {
      ...SpaceBasicInfos
      members {
        ...UserBasicInfos
      }
      notMembers {
        ...UserBasicInfos
      }
      directChannels {
        id
        user {
          ...UserBasicInfos
        }
        channel {
          ...ChannelInfos
        }
      }
    }
  }
  ${SpaceBasicInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
  ${ChannelInfosFragmentDoc}
`;
export type InviteUserToSpaceMutationFn = Apollo.MutationFunction<
  InviteUserToSpaceMutation,
  InviteUserToSpaceMutationVariables
>;

/**
 * __useInviteUserToSpaceMutation__
 *
 * To run a mutation, you first call `useInviteUserToSpaceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInviteUserToSpaceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [inviteUserToSpaceMutation, { data, loading, error }] = useInviteUserToSpaceMutation({
 *   variables: {
 *      inputCreateInviteUser: // value for 'inputCreateInviteUser'
 *   },
 * });
 */
export function useInviteUserToSpaceMutation(
  baseOptions?: Apollo.MutationHookOptions<
    InviteUserToSpaceMutation,
    InviteUserToSpaceMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<InviteUserToSpaceMutation, InviteUserToSpaceMutationVariables>(
    InviteUserToSpaceDocument,
    options
  );
}
export type InviteUserToSpaceMutationHookResult = ReturnType<typeof useInviteUserToSpaceMutation>;
export type InviteUserToSpaceMutationResult = Apollo.MutationResult<InviteUserToSpaceMutation>;
export type InviteUserToSpaceMutationOptions = Apollo.BaseMutationOptions<
  InviteUserToSpaceMutation,
  InviteUserToSpaceMutationVariables
>;
export const GetMySpacesDocument = gql`
  query GetMySpaces {
    me {
      id
      joinSpaces {
        ...SpaceBasicInfos
        members {
          id
        }
      }
    }
  }
  ${SpaceBasicInfosFragmentDoc}
`;

/**
 * __useGetMySpacesQuery__
 *
 * To run a query within a React component, call `useGetMySpacesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMySpacesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMySpacesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetMySpacesQuery(
  baseOptions?: Apollo.QueryHookOptions<GetMySpacesQuery, GetMySpacesQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetMySpacesQuery, GetMySpacesQueryVariables>(GetMySpacesDocument, options);
}
export function useGetMySpacesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetMySpacesQuery, GetMySpacesQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetMySpacesQuery, GetMySpacesQueryVariables>(
    GetMySpacesDocument,
    options
  );
}
export type GetMySpacesQueryHookResult = ReturnType<typeof useGetMySpacesQuery>;
export type GetMySpacesLazyQueryHookResult = ReturnType<typeof useGetMySpacesLazyQuery>;
export type GetMySpacesQueryResult = Apollo.QueryResult<
  GetMySpacesQuery,
  GetMySpacesQueryVariables
>;
export const GetSpaceChannelsDocument = gql`
  query GetSpaceChannels($id: String!) {
    space(id: $id) {
      ...SpaceBasicInfos
      members {
        ...UserBasicInfos
      }
      channels {
        ...ChannelInfos
      }
      directChannels {
        id
        user {
          ...UserBasicInfos
        }
        channel {
          ...ChannelInfos
        }
      }
    }
  }
  ${SpaceBasicInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
  ${ChannelInfosFragmentDoc}
`;

/**
 * __useGetSpaceChannelsQuery__
 *
 * To run a query within a React component, call `useGetSpaceChannelsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSpaceChannelsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSpaceChannelsQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetSpaceChannelsQuery(
  baseOptions: Apollo.QueryHookOptions<GetSpaceChannelsQuery, GetSpaceChannelsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetSpaceChannelsQuery, GetSpaceChannelsQueryVariables>(
    GetSpaceChannelsDocument,
    options
  );
}
export function useGetSpaceChannelsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetSpaceChannelsQuery, GetSpaceChannelsQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetSpaceChannelsQuery, GetSpaceChannelsQueryVariables>(
    GetSpaceChannelsDocument,
    options
  );
}
export type GetSpaceChannelsQueryHookResult = ReturnType<typeof useGetSpaceChannelsQuery>;
export type GetSpaceChannelsLazyQueryHookResult = ReturnType<typeof useGetSpaceChannelsLazyQuery>;
export type GetSpaceChannelsQueryResult = Apollo.QueryResult<
  GetSpaceChannelsQuery,
  GetSpaceChannelsQueryVariables
>;
export const GetSpaceMembersDocument = gql`
  query GetSpaceMembers($id: String!) {
    space(id: $id) {
      ...SpaceBasicInfos
      members {
        ...UserBasicInfos
      }
      notMembers {
        ...UserBasicInfos
      }
    }
  }
  ${SpaceBasicInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useGetSpaceMembersQuery__
 *
 * To run a query within a React component, call `useGetSpaceMembersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSpaceMembersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSpaceMembersQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetSpaceMembersQuery(
  baseOptions: Apollo.QueryHookOptions<GetSpaceMembersQuery, GetSpaceMembersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetSpaceMembersQuery, GetSpaceMembersQueryVariables>(
    GetSpaceMembersDocument,
    options
  );
}
export function useGetSpaceMembersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetSpaceMembersQuery, GetSpaceMembersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetSpaceMembersQuery, GetSpaceMembersQueryVariables>(
    GetSpaceMembersDocument,
    options
  );
}
export type GetSpaceMembersQueryHookResult = ReturnType<typeof useGetSpaceMembersQuery>;
export type GetSpaceMembersLazyQueryHookResult = ReturnType<typeof useGetSpaceMembersLazyQuery>;
export type GetSpaceMembersQueryResult = Apollo.QueryResult<
  GetSpaceMembersQuery,
  GetSpaceMembersQueryVariables
>;
export const GetSpaceInfoDocument = gql`
  query GetSpaceInfo($id: String!) {
    space(id: $id) {
      ...SpaceBasicInfos
    }
  }
  ${SpaceBasicInfosFragmentDoc}
`;

/**
 * __useGetSpaceInfoQuery__
 *
 * To run a query within a React component, call `useGetSpaceInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSpaceInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSpaceInfoQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetSpaceInfoQuery(
  baseOptions: Apollo.QueryHookOptions<GetSpaceInfoQuery, GetSpaceInfoQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetSpaceInfoQuery, GetSpaceInfoQueryVariables>(
    GetSpaceInfoDocument,
    options
  );
}
export function useGetSpaceInfoLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetSpaceInfoQuery, GetSpaceInfoQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetSpaceInfoQuery, GetSpaceInfoQueryVariables>(
    GetSpaceInfoDocument,
    options
  );
}
export type GetSpaceInfoQueryHookResult = ReturnType<typeof useGetSpaceInfoQuery>;
export type GetSpaceInfoLazyQueryHookResult = ReturnType<typeof useGetSpaceInfoLazyQuery>;
export type GetSpaceInfoQueryResult = Apollo.QueryResult<
  GetSpaceInfoQuery,
  GetSpaceInfoQueryVariables
>;
export const UpdateUserDocument = gql`
  mutation UpdateUser($id: String!, $input: UserProfileInput!) {
    updateProfile(id: $id, input: $input) {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;
export type UpdateUserMutationFn = Apollo.MutationFunction<
  UpdateUserMutation,
  UpdateUserMutationVariables
>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserMutation(
  baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(
    UpdateUserDocument,
    options
  );
}
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<
  UpdateUserMutation,
  UpdateUserMutationVariables
>;
export const MeDocument = gql`
  query Me {
    me {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export function useMeLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const SubscribeToUserStatusDocument = gql`
  subscription SubscribeToUserStatus {
    updateUser {
      id
      isOnline
      lastConnectedTime
    }
  }
`;

/**
 * __useSubscribeToUserStatusSubscription__
 *
 * To run a query within a React component, call `useSubscribeToUserStatusSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToUserStatusSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToUserStatusSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToUserStatusSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToUserStatusSubscription,
    SubscribeToUserStatusSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToUserStatusSubscription,
    SubscribeToUserStatusSubscriptionVariables
  >(SubscribeToUserStatusDocument, options);
}
export type SubscribeToUserStatusSubscriptionHookResult = ReturnType<
  typeof useSubscribeToUserStatusSubscription
>;
export type SubscribeToUserStatusSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToUserStatusSubscription>;

export interface PossibleTypesResultData {
  possibleTypes: {
    [key: string]: string[];
  };
}
const result: PossibleTypesResultData = {
  possibleTypes: {},
};
export default result;
