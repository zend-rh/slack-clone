import { gql } from '@apollo/client';

export const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        id
      }
    }
  }
`;

export const SIGNUP = gql`
  mutation SignUp($userInput: UserCreateInput!) {
    signUp(userInput: $userInput) {
      token
      user {
        id
      }
    }
  }
`;
