import { gql } from '@apollo/client';

export const CHANNEL_FRAGMENT = gql`
  fragment ChannelInfos on Channel {
    id
    name
    authorId
    spaceId
    createdAt
    updatedAt
    isDeleted
    isPrivate
  }
`;
