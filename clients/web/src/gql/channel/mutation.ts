import { gql } from '@apollo/client';
import { CHANNEL_FRAGMENT } from './fragment';

export const CREATE_CHANNEL = gql`
  mutation CreateChannel($inputCreateChannel: InputCreateChannel!) {
    createChannel(inputCreateChannel: $inputCreateChannel) {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;
export const UPDATE_CHANNEL = gql`
  mutation UpdateChannel($inputUpdateChannel: InputUpdateChannel!) {
    updateChannel(inputUpdateChannel: $inputUpdateChannel) {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;

export const DELETE_CHANNEL = gql`
  mutation DeleteChannel($id: String!) {
    deleteChannel(id: $id) {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;
