import { gql } from '@apollo/client';
import { MESSAGE_FRAGMENT } from '../message/fragment';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { CHANNEL_FRAGMENT } from './fragment';

export const GET_CHANNEL_MESSAGES = gql`
  query GetChannelMessages($id: String!) {
    channel(id: $id) {
      ...ChannelInfos
      messages {
        ...MessageInfos
        author {
          ...UserBasicInfos
        }
      }
    }
  }
  ${CHANNEL_FRAGMENT}
  ${MESSAGE_FRAGMENT}
  ${USER_BASIC_FRAGMENT}
`;

export const GET_CHANNEL_MEMBERS = gql`
  query GetChannelMembers($id: String!) {
    channel(id: $id) {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${CHANNEL_FRAGMENT}
`;
