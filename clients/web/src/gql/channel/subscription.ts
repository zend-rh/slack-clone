import { gql } from '@apollo/client';
import { CHANNEL_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_NEW_CHANNEL_ADDED = gql`
  subscription SubscribeToNewChannel($spaceId: String!) {
    newChannel(spaceId: $spaceId) {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;
export const SUBSCRIBE_TO_EDIT_CHANNEL_ADDED = gql`
  subscription SubscribeToEditChannel($spaceId: String!) {
    editChannel(spaceId: $spaceId) {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;
export const SUBSCRIBE_TO_DELETE_CHANNEL_ADDED = gql`
  subscription SubscribeToDeleteChannel($spaceId: String!) {
    deleteChannel(spaceId: $spaceId) {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;
