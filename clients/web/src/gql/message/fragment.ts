import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';

export const MESSAGE_FRAGMENT = gql`
  fragment MessageInfos on Message {
    id
    text
    createdAt
    updatedAt
    isMine
    isDeleted
    authorId
    channelId
    author {
      ...UserBasicInfos
    }
  }
  ${USER_BASIC_FRAGMENT}
`;
