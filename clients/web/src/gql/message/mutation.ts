import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { MESSAGE_FRAGMENT } from './fragment';

export const CREATE_MESSAGE = gql`
  mutation CreateMessage($inputCreateMessage: InputCreateMessage!) {
    createMessage(inputCreateMessage: $inputCreateMessage) {
      ...MessageInfos
      author {
        ...UserBasicInfos
      }
    }
  }
  ${MESSAGE_FRAGMENT}
  ${USER_BASIC_FRAGMENT}
`;
export const UPDATE_MESSAGE = gql`
  mutation UpdateMessage($inputUpdateMessage: InputUpdateMessage!) {
    updateMessage(inputUpdateMessage: $inputUpdateMessage) {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;

export const DELETE_MESSAGE = gql`
  mutation DeleteMessage($id: String!) {
    deleteMessage(id: $id) {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;
