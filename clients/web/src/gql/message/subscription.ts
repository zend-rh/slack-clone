import { gql } from '@apollo/client';
import { MESSAGE_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_NEW_MESSAGE_ADDED = gql`
  subscription SubscribeToNewMessage($channelId: String!) {
    newMessage(channelId: $channelId) {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;
export const SUBSCRIBE_TO_EDIT_MESSAGE_ADDED = gql`
  subscription SubscribeToEditMessage($channelId: String!) {
    editMessage(channelId: $channelId) {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;
export const SUBSCRIBE_TO_DELETE_MESSAGE_ADDED = gql`
  subscription SubscribeToDeleteMessage($channelId: String!) {
    deleteMessage(channelId: $channelId) {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;
