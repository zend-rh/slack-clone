import { gql } from '@apollo/client';

export const SPACE_BASIC_FRAGMENT = gql`
  fragment SpaceBasicInfos on Space {
    id
    name
    isDeleted
    createdAt
    updatedAt
  }
`;
