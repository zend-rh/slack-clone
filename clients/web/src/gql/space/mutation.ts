import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { SPACE_BASIC_FRAGMENT } from './fragment';

export const CREATE_SPACE = gql`
  mutation CreateSpace($inputCreateSpace: InputCreateSpace!) {
    createSpace(inputCreateSpace: $inputCreateSpace) {
      ...SpaceBasicInfos
    }
  }
  ${SPACE_BASIC_FRAGMENT}
`;

export const INVITE_USER_TO_SPACE = gql`
  mutation InviteUserToSpace($inputCreateInviteUser: InputCreateInviteUser!) {
    createInviteUser(inputCreateInviteUser: $inputCreateInviteUser) {
      ...SpaceBasicInfos
      members {
        ...UserBasicInfos
      }
      notMembers {
        ...UserBasicInfos
      }
      directChannels {
        id
        user {
          ...UserBasicInfos
        }
        channel {
          ...ChannelInfos
        }
      }
    }
  }
  ${SPACE_BASIC_FRAGMENT}
  ${USER_BASIC_FRAGMENT}
`;
