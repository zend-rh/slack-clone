import { gql } from '@apollo/client';
import { CHANNEL_FRAGMENT } from '../channel/fragment';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { SPACE_BASIC_FRAGMENT } from './fragment';

export const GET_MY_SPACES = gql`
  query GetMySpaces {
    me {
      id
      joinSpaces {
        ...SpaceBasicInfos
        members {
          id
        }
      }
    }
  }
  ${SPACE_BASIC_FRAGMENT}
`;

export const GET_SPACE_CHANNELS = gql`
  query GetSpaceChannels($id: String!) {
    space(id: $id) {
      ...SpaceBasicInfos
      members {
        ...UserBasicInfos
      }
      channels {
        ...ChannelInfos
      }
      directChannels {
        id
        user {
          ...UserBasicInfos
        }
        channel {
          ...ChannelInfos
        }
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${SPACE_BASIC_FRAGMENT}
  ${CHANNEL_FRAGMENT}
`;
export const GET_SPACE_MEMBERS = gql`
  query GetSpaceMembers($id: String!) {
    space(id: $id) {
      ...SpaceBasicInfos
      members {
        ...UserBasicInfos
      }
      notMembers {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${SPACE_BASIC_FRAGMENT}
`;

export const GET_SPACE_INFO = gql`
  query GetSpaceInfo($id: String!) {
    space(id: $id) {
      ...SpaceBasicInfos
    }
  }
  ${SPACE_BASIC_FRAGMENT}
`;
