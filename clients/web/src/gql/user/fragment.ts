import { gql } from '@apollo/client';

export const USER_BASIC_FRAGMENT = gql`
  fragment UserBasicInfos on User {
    id
    email
    avatar
    firstName
    lastName
    displayName
    isOnline
    lastConnectedTime
    isMe
    createdAt
    updatedAt
  }
`;
