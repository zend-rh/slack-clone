import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from './fragment';

export const ME = gql`
  query Me {
    me {
      ...UserBasicInfos
    }
  }
  ${USER_BASIC_FRAGMENT}
`;
