import { gql } from '@apollo/client';

export const SUBSCRIBE_TO_USER_STATUS = gql`
  subscription SubscribeToUserStatus {
    updateUser {
      id
      isOnline
      lastConnectedTime
    }
  }
`;

// export const SUBSCRIBE_TO_USER_LOGGED_OUT = gql`
//   subscription SubscribeToUserLoggedOut {
//     userLoggedOut {
//       id
//       lastConnection
//     }
//   }
//   ${USER_BASIC_FRAGMENT}
// `;

// export const SUBSCRIBE_TO_USER_Infos_UPDATED = gql`
//   subscription SubscribeToUserInfosUpdated {
//     userInfoUpdated {
//       ...UserBasicInfos
//     }
//   }
//   ${USER_BASIC_FRAGMENT}
// `;
