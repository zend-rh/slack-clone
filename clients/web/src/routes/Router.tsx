import { lazy } from 'react';
import { Switch } from 'react-router-dom';
import { ProtectedLazyRoute } from '../common/ProtectedLazyRoute';

const Authentication = lazy(() => import('../components/Authentication/Authentication'));
const SignUp = lazy(() => import('../components/SignUp/SignUp'));
const Dashboard = lazy(() => import('../components/Dashboard/Dashboard'));
const SetupSpace = lazy(() => import('../components/SetupSpace/SetupSpace'));
const Home = lazy(() => import('../components/Home/Home'));
const PageNotFound = lazy(() => import('../components/PageNotFound/PageNotFound'));

const Router = () => {
  return (
    <Switch>
      <ProtectedLazyRoute
        path="/login"
        exact={true}
        access="authentication"
        component={Authentication}
      />
      <ProtectedLazyRoute path="/signup" exact={true} access="authentication" component={SignUp} />
      <ProtectedLazyRoute exact={true} path="/" component={Dashboard} />
      <ProtectedLazyRoute
        exact={true}
        path={[
          '/space/:spaceId',
          '/space/:spaceId/channel/:channelId',
          '/space/:spaceId/channel',
          '/space/:spaceId/user/:channelId',
          '/space/:spaceId/user',
        ]}
        component={Home}
      />
      <ProtectedLazyRoute exact={true} path="/setup" component={SetupSpace} />
      <ProtectedLazyRoute path="*" exact={true} component={PageNotFound} access="public" />
    </Switch>
  );
};

export default Router;
