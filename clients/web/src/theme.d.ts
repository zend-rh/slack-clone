import { boxShadow } from "./themes/myTheme/boxShadows";
import colors from "./themes/myTheme/colors";
import { containerStyles } from "./themes/myTheme/containerStyles";
import { fontFamily } from "./themes/myTheme/fontFamily";
import { textStyles } from "./themes/myTheme/textStyles";

declare module "@mui/material/styles/createTheme" {
  interface Theme {
    colors: typeof colors;
    textStyles: typeof textStyles;
    containerStyles: typeof containerStyles;
    fontFamily: typeof fontFamily;
    boxShadow: typeof boxShadow;
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface ThemeOptions extends Theme {}
}
