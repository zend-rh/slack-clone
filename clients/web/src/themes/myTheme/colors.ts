const colors = {
  white: '#FFFFFF',
  black: '#000000',
  red: '#FF0000',
  gray: '#BDBDBD',
  pink: '#E91E63',
  grape: '#4F004B',
  codGray: '#1d1d1d',
  toryBlue: '#1264a3',
  pictonBlue: '#36c5f0',
  valentino: '#350d36',
  jungleGreen: '#2BAC76',
  transparent: 'transparent',
  apple: '#4BB543',
  orange: 'orange',
  shark: '#1D1C1D',
};

export default colors;
