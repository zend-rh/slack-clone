export const fontFamily = {
  main: 'Slack-Circular-Pro,"Helvetica Neue",Helvetica,"Segoe UI",Tahoma,Arial,sans-serif;',
  secondary: 'Roboto, sans-serif',
};
