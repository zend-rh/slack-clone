import { createTheme } from '@mui/material/styles';
import { boxShadow } from './boxShadows';
import colors from './colors';
import { containerStyles } from './containerStyles';
import { fontFamily } from './fontFamily';
import { textStyles } from './textStyles';

const myTheme = createTheme({
  textStyles,
  containerStyles,
  colors,
  fontFamily,
  boxShadow,
  palette: {
    primary: {
      main: colors.grape,
      light: colors.grape,
      dark: colors.grape,
    },
    secondary: {
      main: colors.shark,
      light: colors.shark,
      dark: colors.shark,
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'capitalize',
        },
      },
    },
    MuiCircularProgress: {
      styleOverrides: {
        colorSecondary: {
          color: colors.pictonBlue,
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        body1: {
          color: colors.black,
        },
        // colorTextPrimary: {
        //   color: colors.black,
        // },
        // colorTextSecondary: {
        //   color: colors.white,
        // },
        // colorSecondary: {
        //   color: colors.toryBlue,
        // },
        // colorInherit: {
        //   color: colors.orange,
        // },
      },
    },
  },
});

export default myTheme;
