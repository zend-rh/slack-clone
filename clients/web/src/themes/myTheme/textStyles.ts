import { createStyles } from '@mui/styles';

export const textStyles = createStyles({
  ellipsis: {
    maxWidth: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
});
