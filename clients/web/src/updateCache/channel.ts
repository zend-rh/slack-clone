import { BaseSubscriptionOptions, DataProxy, MutationUpdaterFn } from '@apollo/client';
import {
  CreateChannelMutation,
  GetSpaceChannelsQuery,
  GetSpaceChannelsQueryVariables,
  SubscribeToNewChannelSubscription,
} from '../generated/graphql';
import { GET_SPACE_CHANNELS } from '../gql/space/query';

export const getChannelsInCache = (cache: DataProxy, spaceId: string) => {
  const queryVariable: DataProxy.Query<GetSpaceChannelsQueryVariables, GetSpaceChannelsQuery> = {
    query: GET_SPACE_CHANNELS,
    variables: {
      id: spaceId,
    },
  };
  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};

export const updateCacheAfterAddingChannel: MutationUpdaterFn<CreateChannelMutation> = (
  cache,
  { data }
) => {
  if (data?.createChannel) {
    const { createChannel } = data;
    updateCacheCommon(cache, createChannel);
  }
};

export const updateCacheAfterSubscribsriptionChannelAdded: BaseSubscriptionOptions<SubscribeToNewChannelSubscription>['onSubscriptionData'] =
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.newChannel) {
      const { newChannel } = subscriptionData.data;
      updateCacheCommon(client, newChannel);
    }
  };

const updateCacheCommon = (
  cache: DataProxy,
  addChannel: CreateChannelMutation['createChannel']
) => {
  if (!addChannel) {
    return;
  }

  const { id, spaceId } = addChannel;

  const { dataCache, queryVariable } = getChannelsInCache(cache, spaceId);

  if (dataCache?.space?.channels) {
    const { channels } = dataCache.space;

    if (!channels.some(ch => ch && ch.id === id)) {
      const newChannels = [addChannel, ...((channels || []) as any)];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          space: {
            ...(dataCache?.space as any),
            channels: newChannels,
          },
        },
      });
    }
  }
};
