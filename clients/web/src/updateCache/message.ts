import { ApolloCache, BaseSubscriptionOptions, DataProxy, MutationUpdaterFn } from '@apollo/client';
import {
  CreateMessageMutation,
  GetChannelMessagesQuery,
  GetChannelMessagesQueryVariables,
  SubscribeToNewMessageSubscription,
} from '../generated/graphql';
import { GET_CHANNEL_MESSAGES } from '../gql/channel/query';

// export const getMessagesInCacheFn = (cache: DataProxy, channelId: string) => {
//   const queryVariable: DataProxy.Query<G, GetUserMessageQuery> = {
//     query: GET_USER_MESSAGES,
//     variables: {
//       filter: {
//         userId: id,import { BaseSubscriptionOptions, DataProxy, MutationUpdaterFn } from '@apollo/client';
//       },
//     },
//   };
//   return { dataCache: cache.readQuery(queryVariable), queryVariable };
// };

export const getChannelMessagesInCacheFn = (cache: DataProxy, channelId: string) => {
  const queryVariable: DataProxy.Query<GetChannelMessagesQueryVariables, GetChannelMessagesQuery> =
    {
      query: GET_CHANNEL_MESSAGES,
      variables: {
        id: channelId,
      },
    };
  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};

export const updateCacheAfterAddingChannelMessage: MutationUpdaterFn<CreateMessageMutation> = (
  cache,
  { data }
) => {
  if (data?.createMessage) {
    const { createMessage } = data;
    updateCacheCommon(cache, createMessage);
  }
};

export const updateCacheCommon = (
  cache: ApolloCache<any>,
  addChannelMessage: CreateMessageMutation['createMessage']
) => {
  if (!addChannelMessage) {
    return;
  }
  const { channelId, id: messageId } = addChannelMessage;

  if (!channelId) {
    return;
  }

  const { dataCache, queryVariable } = getChannelMessagesInCacheFn(cache, channelId);

  if (dataCache?.channel?.messages) {
    const { messages } = dataCache.channel;

    if (!messages.some(ms => ms && ms.id === messageId)) {
      const newMessages = [...messages, addChannelMessage];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          channel: {
            ...dataCache.channel,
            messages: newMessages,
          },
        },
      });
    }
  }
};

export const updateCacheAfterSubscribsriptionMessageAdded: BaseSubscriptionOptions<SubscribeToNewMessageSubscription>['onSubscriptionData'] =
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.newMessage) {
      const { newMessage } = subscriptionData.data;
      const { channelId } = newMessage;
      if (channelId) {
        updateCacheAfterAddingChannelMessage(client as any, {
          data: { createMessage: newMessage },
        });
      }
      //  else if (userId) {
      //   updateCacheAfterAddingDiscussionMessage(client as any, {
      //     data: { addDiscussionMessage: messageAdded },
      //   });
      // }
    }
  };
