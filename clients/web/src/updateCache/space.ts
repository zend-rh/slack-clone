import { DataProxy, MutationUpdaterFn } from '@apollo/client';
import {
  CreateSpaceMutation,
  GetMySpacesQuery,
  GetMySpacesQueryVariables,
} from '../generated/graphql';
import { GET_MY_SPACES } from '../gql/space/query';

export const getSpacesInCache = (cache: DataProxy) => {
  const queryVariable: DataProxy.Query<GetMySpacesQueryVariables, GetMySpacesQuery> = {
    query: GET_MY_SPACES,
  };
  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};

export const updateCacheAfterAddingSpace: MutationUpdaterFn<CreateSpaceMutation> = (
  cache,
  { data }
) => {
  if (data?.createSpace) {
    const { createSpace } = data;
    updateCacheCommon(cache, createSpace);
  }
};

// export const updateCacheAfterSubscribsriptionChannelAdded: BaseSubscriptionOptions<SubscribeToNewChannelSubscription>['onSubscriptionData'] =
//   ({ client, subscriptionData }) => {
//     if (subscriptionData.data?.newChannel) {
//       const { newChannel } = subscriptionData.data;
//       updateCacheCommon(client, newChannel);
//     }
//   };

const updateCacheCommon = (cache: DataProxy, createSpace: CreateSpaceMutation['createSpace']) => {
  if (!createSpace) {
    return;
  }

  const { id } = createSpace;

  const { dataCache, queryVariable } = getSpacesInCache(cache);

  if (dataCache?.me?.joinSpaces) {
    const { joinSpaces } = dataCache.me;

    if (!joinSpaces.some(ch => ch && ch.id === id)) {
      const newSPpaces = [createSpace, ...((joinSpaces || []) as any)];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          me: {
            ...(dataCache?.me as any),
            joinSpaces: newSPpaces,
          },
        },
      });
    }
  }
};
