export const important = (cssValue: string) => `${cssValue}!important`;
