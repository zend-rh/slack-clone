import { UserBasicInfosFragment } from '../generated/graphql';

const toLocalUppercase = (str: string) => `${str.substr(0, 1).toUpperCase()}${str.substr(1)}`;

export const getUserName = (user: UserBasicInfosFragment) => {
  return `${toLocalUppercase(user?.firstName || '')} ${toLocalUppercase(user?.lastName || '')}`;
};
