FROM node:12-alpine

WORKDIR /graphql

COPY package.json /graphql/package.json
COPY yarn.lock /graphql/

# Install packages
RUN yarn install

COPY . /graphql/

EXPOSE 4000
EXPOSE 9229

ENV POSTGRES_HOST postgres
ENV POSTGRES_PORT 5432

CMD ./wait-for-it.sh -t 30 -h $POSTGRES_HOST -p $POSTGRES_PORT -- yes | yarn generate && yarn migrate && yarn seed && yarn start:dev
