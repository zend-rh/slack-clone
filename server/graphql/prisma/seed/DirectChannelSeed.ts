import { PrismaClient } from "@prisma/client";
import  { uniqBy } from 'lodash';
import { hashSync } from "bcrypt";

export const combination = <T>(
  tab: T[]
): {
  a: T;
  b: T;
}[] => {
  let results: {
    a: T;
    b: T;
  }[] = [];
  for (let i = 0; i < tab.length - 1; i++) {
    for (let j = i + 1; j < tab.length; j++) {
      results.push({
        a: tab[i],
        b: tab[j],
      });
    }
  }
  return results;
};

export const DirectChannelSeed = async (prisma: PrismaClient) => {
  const allSpaces = await prisma.space.findMany({
    include: {
      userSpaces: true,
    },
  });
  const e = await Promise.all(
    allSpaces.map(async (space) => {
      const filterUniq = uniqBy(space.userSpaces, ({userId})=> userId);
      const allCombinaison = combination(filterUniq);
      await Promise.all(
        allCombinaison.map(async (i) => {
          let findExist = await prisma.channel.findFirst({
            where: {
              name: {
                in: [
                  `${space.id}:${i.a.userId}:${i.b.userId}`,
                  `${space.id}:${i.b.userId}:${i.a.userId}`,
                ],
              },
              spaceId: space.id,
              isDirectChannel: true,
              isPrivate: false,
            },
          });
          if (!findExist) {
            findExist = await prisma.channel.create({
              data: {
                isPrivate: false,
                isDirectChannel: true,
                spaceId: space.id,
                name: `${space.id}:${i.a.userId}:${i.b.userId}`,
              },
            });
          }
          if (!findExist?.id) return [];
          const userChannels = await Promise.all(
            [i.a.userId, i.b.userId].map(async (e) => {
              const find = await prisma.userChannel.findFirst({
                where: {
                  channelId: findExist?.id || "",
                  userId: e,
                },
              });
              if (!find) {
                const userChannels = await prisma.userChannel.create({
                  data: {
                    channelId: findExist?.id || "",
                    userId: e,
                  },
                });
                return userChannels;
              }
              return find;
            })
          );

          return userChannels;
        })
      );
    })
  );
  console.log("Seed direct messages");
  return e;
};
