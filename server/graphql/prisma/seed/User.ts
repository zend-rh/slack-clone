import { PrismaClient } from '@prisma/client';
import { hashSync } from 'bcrypt';

export const UserSeed = async (prisma: PrismaClient) => {
  const data =  [{
    email: 'kevin@gmail.com',
    firstName: 'Kevin',
    lastName: 'Martin',
    password: hashSync('kevin@1234', 10),
  },{
    email: 'julien@gmail.com',
    firstName: 'Julien',
    lastName: 'Bejamin',
    password: hashSync('julien@1234', 10),
  }]
  const res = await Promise.all(data.map(async i=> {
    return  prisma.user.upsert({
      where: {
        email: i.email,
      },
      create: {
        email: i.email,
        lastName: i.lastName,
        password: i.password,
        firstName: i.firstName,
      },
      update: {
        updatedAt: new Date(),
      }
    })
  }));
  console.log("User seed finish");
  return res;
};
