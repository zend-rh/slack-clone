import { PrismaClient } from '@prisma/client';
import { UserSeed } from './User';
import { DirectChannelSeed } from './DirectChannelSeed';

const allSeed = async (prisma: PrismaClient) => {
  await UserSeed(prisma);
  await DirectChannelSeed(prisma);
};

export default allSeed;
