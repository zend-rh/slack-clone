import { PrismaClient, User } from "@prisma/client";
import { ExpressContext } from "apollo-server-express/dist/ApolloServer";
import { PubSub } from "graphql-subscriptions";
import Redis from "ioredis";
import { Context } from "../types/contextType";
import { PubSubType } from "./redis";

export const getUserByToken = async (
  token: string | undefined,
  redis: Redis.Redis
) => {
  if (token) {
    const id = await redis.get(token);
    if (id) {
      return prisma.user.findUnique({ where: { id } });
    }
  }
  return null;
};

export const getCurrentUser = async (
  req: any,
  prisma: PrismaClient,
  redis: Redis.Redis
): Promise<User | null> => {
  if (req && req.headers && req.headers.authorization) {
    const accessToken = req.headers.authorization.replace("Bearer ", "");
    return getUserByToken(accessToken, redis);
  }
  return null;
};

export const prisma = new PrismaClient();

export const createContext = async (
  { req }: ExpressContext,
  redis: Redis.Redis,
  pubsub: PubSubType
): Promise<Context> => {
  const currentUser = await getCurrentUser(req, prisma, redis);

  return {
    prisma,
    pubsub,
    currentUser,
    userId: currentUser ? currentUser.id : "-1",
    redis,
  };
};
export const createContextWS = async (
  {
    authToken,
  }: {
    authToken?: string;
  },
  redis: Redis.Redis,
  pubsub: PubSubType,
  status: "online" | "offline"
): Promise<Context> => {
  const currentUser = await getUserByToken(authToken, redis);
  await setStatus(currentUser, redis, pubsub, status);
  return {
    prisma,
    pubsub,
    currentUser,
    userId: currentUser ? currentUser.id : "-1",
    redis,
  };
};

export const setStatus = async (
  user: User | null,
  redis: Redis.Redis,
  pubsub: PubSubType,
  status: "online" | "offline"
) => {
  if (user) {
    console.log("user ", user.email, " ", status);
    const statusKey = `user:${user.id}:lastSeen`;
    const lastSeen = status === "online" ? 0 : new Date().getTime();
    const res = await redis.set(statusKey, lastSeen);
    await pubsub.publish("updateUser", {
      updateUser: {
        ...user,
        isOnline: status === "online",
        lastConnectedTime: lastSeen,
      },
    });
    return res === "OK";
  }
  return false;
};
