import { PubSub } from "graphql-subscriptions";
import Redis from "ioredis";
import { NexusGenFieldTypes } from "../schema/generated/nexus";

export type PubSubType = Omit<PubSub, "publish"> & {
  publish: <
    T extends keyof NexusGenFieldTypes["Subscription"],
    PayloadType = unknown
  >(
    trigger: T,
    payload: PayloadType & Record<T, any>
  ) => Promise<void>;
};

export const configureRedis = (): {
  redis: Redis.Redis;
  pubsub: PubSubType;
} => {
  const options = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT ? +process.env.REDIS_PORT : 6379,
    retryStrategy: (times: number) => {
      // reconnect after
      return Math.min(times * 50, 2000);
    },
  };

  const pubsubI = new PubSub();
  const redis = new Redis(options);
  const pubsub: PubSubType = pubsubI;
  return { redis, pubsub };
};
