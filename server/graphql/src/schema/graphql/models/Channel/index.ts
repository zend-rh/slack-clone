import { list, nonNull, objectType } from "nexus";

export const Channel = objectType({
  name: "Channel",
  definition(t) {
    t.nonNull.id("id");
    t.nonNull.string("name");
    t.nonNull.string("spaceId");
    t.string("authorId");
    t.boolean("isPrivate");
    t.boolean("isDirectChannel");
    t.boolean("isDeleted");
    t.field("createdAt", {
      type: "DateTime",
    });
    t.field("updatedAt", {
      type: "DateTime",
    });

    t.field("messages", {
      type: list("Message"),
      resolve: async (parent, _, ctx) => {
        return ctx.prisma.message.findMany({
          where: {
            channelId: parent.id,
          },
        });
      },
    });
    t.field("author", {
      type: "User",
      resolve: async (parent, _, ctx) => {
        if (parent?.authorId)
          return ctx.prisma.user.findUnique({
            where: {
              id: parent?.authorId,
            },
          });
        return null;
      },
    });
    t.field("space", {
      type: nonNull("Space"),
      resolve: async (parent, _, ctx) => {
        return (await ctx.prisma.space.findUnique({
          where: {
            id: parent.spaceId,
          },
        })) as any;
      },
    });
    t.field("members", {
      type: list(nonNull("User")),
      resolve: async (parent, _, ctx) => {
        const res = await ctx.prisma.user.findMany({
          where: {
            userChannels: {
              every: {
                channelId: parent.id,
              },
            },
          },
        });
        return res;
      },
    });
  },
});
