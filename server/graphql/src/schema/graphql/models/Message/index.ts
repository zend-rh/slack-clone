import { nonNull, objectType } from "nexus";

export const Message = objectType({
  name: "Message",
  definition(t) {
    t.nonNull.id("id");
    t.string("text");
    t.nonNull.string("channelId");
    t.string("authorId");
    t.boolean("isDeleted");
    t.field("createdAt", {
      type: "DateTime",
    });
    t.field("updatedAt", {
      type: "DateTime",
    });
    t.field('isMine', {
      type: 'Boolean',
      resolve: (parent,_,ctx)=>{
        return parent.authorId === ctx.currentUser?.id
      }
    })
    t.field('author', {
      type: 'User',
      resolve: async (parent,_, ctx)=> {
        return ctx.prisma.user.findUnique({
          where: {
            id: parent?.authorId
          }
        })
      }
    })
    t.field('channel', {
      type: nonNull('Channel'),
      resolve: async (parent,_, ctx)=> {
        return ctx.prisma.channel.findUnique({
          where: {
            id: parent.channelId
          }
        })
      }
    })
  },
});
