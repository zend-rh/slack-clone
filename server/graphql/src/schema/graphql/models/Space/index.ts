import { list, nonNull, objectType } from "nexus";

export const DirectChannel = objectType({
  name: "DirectChannel",
  definition(t) {
    t.nonNull.string('id');
    t.field("user", {
      type: "User",
    });
    t.field("channel", {
      type: "Channel",
    });
  },
});

export const Space = objectType({
  name: "Space",
  definition(t) {
    t.nonNull.id("id");
    t.nonNull.string("name");
    t.boolean("isDeleted");
    t.field("createdAt", {
      type: "DateTime",
    });
    t.field("updatedAt", {
      type: "DateTime",
    });
    t.field("directChannels", {
      type: list("DirectChannel"),
      resolve: async (parent, _, ctx) => {
        try {
          const res = await ctx.prisma.channel.findMany({
            where: {
              isDirectChannel: true,
              spaceId: parent.id,
              userChannels: {
                some: {
                  userId: ctx.userId,
                },
              },
            },
            include: {
              userChannels: {
                include: {
                  user: true,
                },
              },
            },
            distinct: 'id',
          });
          return res
            .filter((e) => e.userChannels.length === 2)
            .map((e) => ({
              id: e.id,
              channel: e,
              user: e.userChannels.find(
                (i) => i.userId !== ctx.currentUser?.id
              )?.user,
            })) as any;
        } catch (error) {
          return [];
        }
      },
    });
    t.field("channels", {
      type: list("Channel"),
      resolve: async (parent, _, ctx) => {
        try {
          const res = await ctx.prisma.channel.findMany({
            where: {
              isDirectChannel: false,
              spaceId: parent.id,
              OR: [
                {
                  isPrivate: true,
                  userChannels: {
                    some: {
                      userId: ctx.userId,
                    },
                  },
                },
                { isPrivate: false },
              ],
            },
          });
          return res as any;
        } catch (error) {
          return error;
        }
      },
    });
    t.field("members", {
      type: list(nonNull("User")),
      resolve: async (parent, _, ctx) => {
        const res = await ctx.prisma.user.findMany({
          where: {
            OR: [
              {
                userChannels: {
                  some: {
                    channel: {
                      spaceId: parent.id,
                    },
                  },
                },
              },
              {
                userSpaces: {
                  some: {
                    spaceId: parent.id,
                  },
                },
              },
            ],
          },
          distinct: "id",
        });
        return res;
      },
    });
    t.field("notMembers", {
      type: list(nonNull("User")),
      resolve: async (parent, _, ctx) => {
        const res = await ctx.prisma.user.findMany({
          where: {
            OR: [
              {
                userChannels: {
                  none: {
                    channel: {
                      spaceId: parent.id,
                    },
                  },
                },
              },
              {
                userSpaces: {
                  none: {
                    spaceId: parent.id,
                  },
                },
              },
            ],
          },
          distinct: "id",
        });
        return res;
      },
    });
  },
});
