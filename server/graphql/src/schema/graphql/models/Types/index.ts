import { objectType } from 'nexus';

export const SuccessReturn = objectType({
  name: 'SuccessReturn',
  definition(t) {
    t.nonNull.boolean('success');
    t.nonNull.string('message');
  },
});
export const AuthUser = objectType({
  name: 'AuthUser',
  definition(t) {
    t.nonNull.field('user', {
      type: 'User',
    });
    t.nonNull.string('token');
  },
});
