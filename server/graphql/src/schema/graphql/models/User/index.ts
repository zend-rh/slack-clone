import { list, nonNull, objectType } from "nexus";

export const User = objectType({
  name: "User",
  definition(t) {
    t.nonNull.id("id");
    t.string("email");
    t.string("firstName");
    t.string("avatar");
    t.string("lastName");
    t.string("displayName");
    t.string("password");
    t.field("createdAt", {
      type: "DateTime",
    });
    t.field("updatedAt", {
      type: "DateTime",
    });
    t.boolean("isRemoved");
    t.boolean("isBanned");
    t.field("isOnline", {
      type: "Boolean",
      resolve: async (parent, _, ctx) => {
        const statusKey = `user:${parent.id}:lastSeen`;
        const res = await ctx.redis.get(statusKey);
        return res === "0";
      },
    });
    t.field("lastConnectedTime", {
      type: "String",
      resolve: async (parent, _, ctx) => {
        const statusKey = `user:${parent.id}:lastSeen`;
        const res = await ctx.redis.get(statusKey);
        return res ? res : null;
      },
    });
    t.field("isMe", {
      type: "Boolean",
      resolve: (parent, _, ctx) => {
        return parent.id === ctx.currentUser?.id;
      },
    });
    t.field("createChannels", {
      type: nonNull(list(nonNull("Channel"))),
      resolve: async (parent, _, ctx) => {
        return ctx.prisma.channel.findMany({
          where: {
            authorId: parent.id,
          },
        });
      },
    });
    t.field("joinChannels", {
      type: nonNull(list(nonNull("Channel"))),
      resolve: async (parent, _, ctx) => {
        return ctx.prisma.channel.findMany({
          where: {
            OR: [
              {
                userChannels: {
                  some: {
                    userId: ctx.userId,
                  },
                },
              },
              {
                space: {
                  userSpaces: {
                    some: {
                      userId: ctx.userId,
                    },
                  },
                },
              },
              {
                authorId: ctx.userId,
              },
            ],
          },
          distinct: "id",
        });
      },
    });
    t.field("joinSpaces", {
      type: nonNull(list(nonNull("Space"))),
      resolve: async (parent, _, ctx) => {
        return ctx.prisma.space.findMany({
          where: {
            OR: [
              {
                userSpaces: {
                  some: {
                    userId: ctx.userId,
                  },
                },
              },
              {
                authorId: ctx.userId,
              },
            ],
          },
          distinct: "id",
        });
      },
    });
  },
});
