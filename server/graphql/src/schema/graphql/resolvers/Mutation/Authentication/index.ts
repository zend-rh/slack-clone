import { compare, hash } from "bcrypt";
import { extendType, inputObjectType, nonNull, stringArg } from "nexus";
import isEmail from "validator/lib/isEmail";
import { RESET_PASSWORD_KEY } from "../../../../../utils/constants";
import { getRandomDigit } from "../../../../../utils/utils";
import crypto = require("crypto");

export const ChangePasswordInput = inputObjectType({
  name: "ChangePasswordInput",
  definition(t) {
    t.nonNull.string("email");
    t.nonNull.string("newPassword");
  },
});

export const ResetPasswordInput = inputObjectType({
  name: "ResetPasswordInput",
  definition(t) {
    t.nonNull.string("token");
    t.nonNull.string("email");
    t.nonNull.string("newPassword");
  },
});

const generateAccessToken = (value = 256) =>
  crypto.randomBytes(value).toString("hex");

export const UserCreateInput = inputObjectType({
  name: "UserCreateInput",
  definition(t) {
    t.nonNull.string("email");
    t.nonNull.string("firstName");
    t.nonNull.string("lastName");
    t.nonNull.string("password");
  },
});

export const UserProfileInput = inputObjectType({
  name: "UserProfileInput",
  definition(t) {
    t.nonNull.string("email");
    t.nonNull.string("firstName");
    t.nonNull.string("lastName");
    t.nonNull.string("displayName");
  },
});

export const AuthenticationMutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("signUp", {
      type: "AuthUser",
      args: {
        userInput: nonNull(UserCreateInput),
      },
      resolve: async (_parent, { userInput }, ctx) => {
        const { password, email } = userInput;
        const findUser = await ctx.prisma.user.findUnique({
          where: {
            email,
          },
        });
        if (findUser && findUser.password) {
          throw new Error("Email is already used");
        }

        const hashedPassword = await hash(password, 10);
        const created = await ctx.prisma.user.upsert({
          where: {
            email,
          },
          create: {
            ...userInput,
            password: hashedPassword,
          },
          update: {
            ...userInput,
            password: hashedPassword,
          },
        });

        const accessToken = generateAccessToken();
        await ctx.redis.set(accessToken, created.id);

        return {
          token: accessToken,
          user: created,
        };
      },
    });
    t.field("login", {
      type: "AuthUser",
      args: {
        email: nonNull(stringArg()),
        password: nonNull(stringArg()),
      },
      resolve: async (_parent, { email, password }, ctx) => {
        const user = await ctx.prisma.user.findUnique({
          where: {
            email,
          },
        });

        if (!user) {
          throw new Error(`invalid-email`);
        }
        const isPasswordValid = await compare(
          password,
          (user.password || "").trim()
        );

        if (!isPasswordValid) {
          throw new Error("invalid-password");
        }
        const accessToken = generateAccessToken();
        await ctx.redis.set(accessToken, user.id);

        return {
          token: accessToken,
          user,
        };
      },
    });
    t.field("changePassword", {
      type: "User",
      args: {
        input: nonNull("ChangePasswordInput"),
      },
      resolve: async (_parent, { input }, ctx) => {
        try {
          const { email, newPassword } = input;
          const user = await ctx.prisma.user.findUnique({
            where: {
              email,
            },
          });
          if (!user) {
            throw new Error("user-not-found");
          }

          const hashedPassword = await hash(newPassword, 10);
          const updatedUser = await ctx.prisma.user.update({
            where: {
              id: user.id,
            },
            data: {
              password: hashedPassword,
            },
          });
          return updatedUser as any;
        } catch (error) {
          return error;
        }
      },
    });
    t.field("resetPassword", {
      type: "SuccessReturn",
      args: {
        input: nonNull("ResetPasswordInput"),
      },
      resolve: async (_parent, { input }, ctx) => {
        const { email, token, newPassword } = input;
        const user = await ctx.prisma.user.findUnique({
          where: { email },
        });
        if (!user) {
          return {
            success: false,
            message: "User not found",
          };
        }
        const tokenRedis = await ctx.redis.hget(RESET_PASSWORD_KEY, user.id);
        if (!tokenRedis) {
          return {
            success: false,
            message: "Code expired",
          };
        }
        if (tokenRedis === token && token.length) {
          const hashedPassword = await hash(newPassword, 10);
          await ctx.prisma.user
            .update({
              where: { id: user.id },
              data: {
                password: hashedPassword,
              },
            })
            .then(async () => {
              ctx.redis.hset(RESET_PASSWORD_KEY, user.id);
            });
          return {
            message: "Password reset",
            success: true,
          };
        }
        return {
          message: "Access refused",
          success: false,
        };
      },
    });
    t.field("forgotPassword", {
      type: "SuccessReturn",
      args: {
        email: nonNull(stringArg()),
      },
      resolve: async (_parent, { email }, ctx) => {
        if (!isEmail(email)) {
          throw new Error("Invalid email");
        }
        const user = await ctx.prisma.user.findUnique({
          where: { email },
        });
        if (!user) {
          return {
            success: false,
            message: "User not found",
          };
        }
        const token = getRandomDigit(6);
        // token expired on 10mn (600s)
        const value = await ctx.redis.set(
          `${RESET_PASSWORD_KEY}:${user.id}`,
          token,
          "EX",
          600
        );
        console.log(token);
        if (!value) {
          return {
            message: "An error is occurred",
            success: false,
          };
        }

        return {
          success: true,
          message: "Mail sent successfully!",
        };
      },
    });
    t.field("checkExpiredToken", {
      type: "SuccessReturn",
      args: {
        email: nonNull(stringArg()),
        token: nonNull(stringArg()),
      },
      resolve: async (_parent, { email, token }, ctx) => {
        const user = await ctx.prisma.user.findUnique({
          where: { email },
        });
        if (!user) {
          return {
            success: false,
            message: "User not found",
          };
        }
        const tokenRedis = await ctx.redis.get(
          `${RESET_PASSWORD_KEY}:${user.id}`
        );
        if (!tokenRedis) {
          return {
            success: false,
            message: "Code expired",
          };
        }
        if (tokenRedis === token) {
          return {
            success: true,
            message: "Valid code",
          };
        }
        return {
          success: false,
          message: "Error code",
        };
      },
    });
    t.field("updateProfile", {
      type: "User",
      args: {
        id: nonNull(stringArg()),
        input: nonNull(UserProfileInput),
      },
      resolve: async (_parent, { id, input }, ctx) => {
        return ctx.prisma.user.update({
          where: { id },
          data: {
            ...input,
          },
        });
      },
    });
    t.field("updatePassword", {
      type: "User",
      args: {
        id: nonNull(stringArg()),
        oldPassword: nonNull(stringArg()),
        newPassword: nonNull(stringArg()),
      },
      resolve: async (_parent, { id, oldPassword, newPassword }, ctx) => {
        const user = await ctx.prisma.user.findUnique({
          where: {
            id,
          },
        });

        if (!user) {
          throw new Error("user-not-found");
        }

        const isPasswordValid = await compare(
          oldPassword,
          (user.password || "").trim()
        );

        if (!isPasswordValid) {
          throw new Error("invalid-password");
        }

        const hashedPassword = await hash(newPassword, 10);
        const updatedUser = await ctx.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            password: hashedPassword,
          },
        });
        return updatedUser;
      },
    });
  },
});
