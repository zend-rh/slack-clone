import { extendType, inputObjectType, nonNull, stringArg } from "nexus";

export const InputCreateChannel = inputObjectType({
  name: "InputCreateChannel",
  definition(t) {
    t.nonNull.string("name");
    t.nonNull.string("spaceId");
    t.boolean("isPrivate");
  },
});

export const InputUpdateChannel = inputObjectType({
  name: "InputUpdateChannel",
  definition(t) {
    t.nonNull.string("id");
    t.string("name");
    t.boolean("isPrivate");
  },
});

export const ChannelMutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createChannel", {
      type: "Channel",
      args: {
        inputCreateChannel: nonNull(InputCreateChannel),
      },
      resolve: async (_parent, { inputCreateChannel }, ctx) => {
        const { name, spaceId, isPrivate } = inputCreateChannel;
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const createChannel = await ctx.prisma.channel.create({
          data: {
            name,
            isPrivate: isPrivate || false,
            spaceId,
            authorId: ctx.currentUser.id,
          },
        });
        ctx.pubsub.publish('newChannel', {
          newChannel: createChannel
        })
        if(createChannel.isPrivate){
          await ctx.prisma.userChannel.create({
            data: {
              channelId: createChannel.id,
              userId: ctx.currentUser.id,
            }
          })
        }
        return createChannel;
      },
    });
    t.field("updateChannel", {
      type: "Channel",
      args: {
        inputUpdateChannel: nonNull(InputUpdateChannel),
      },
      resolve: async (_parent, { inputUpdateChannel }, ctx) => {
        const { name, id, isPrivate } = inputUpdateChannel;
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const find = await ctx.prisma.channel.findUnique({
          where: {
            id,
          },
        });
        const updated = await ctx.prisma.channel.update({
          where: {
            id,
          },
          data: {
            name: name || find?.name,
            isPrivate: isPrivate || find?.isPrivate,
          },
        });
        ctx.pubsub.publish('editChannel', {
          editChannel: updated
        })
        return updated;
      },
    });
    t.field("deleteChannel", {
      type: "Channel",
      args: {
        id: nonNull(stringArg()),
      },
      resolve: async (_parent, { id }, ctx) => {
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const deleted = await ctx.prisma.channel.update({
          where: {
            id,
          },
          data: {
            isDeleted: true,
          },
        });
        ctx.pubsub.publish('deleteChannel', {
          deleteChannel: deleted
        })
        return deleted;
      },
    });
  },
});
