import { extendType, inputObjectType, nonNull, stringArg } from "nexus";

export const InputCreateMessage = inputObjectType({
  name: "InputCreateMessage",
  definition(t) {
    t.nonNull.string("text");
    t.nonNull.string("channelId");
  },
});

export const InputUpdateMessage = inputObjectType({
  name: "InputUpdateMessage",
  definition(t) {
    t.nonNull.string("id");
    t.string("text");
  },
});

export const MessageMutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createMessage", {
      type: "Message",
      args: {
        inputCreateMessage: nonNull(InputCreateMessage),
      },
      resolve: async (_parent, { inputCreateMessage }, ctx) => {
        const { channelId, text } = inputCreateMessage;
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const createMessage = await ctx.prisma.message.create({
          data: {
            text,
            channelId,
            authorId: ctx.currentUser.id,
          },
        });
        ctx.pubsub.publish('newMessage', {
          newMessage: createMessage
        })
        return createMessage;
      },
    });
    t.field("updateMessage", {
      type: "Message",
      args: {
        inputUpdateMessage: nonNull(InputUpdateMessage),
      },
      resolve: async (_parent, { inputUpdateMessage }, ctx) => {
        const { text, id } = inputUpdateMessage;
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const find = await ctx.prisma.message.findUnique({
          where: {
            id,
          },
        });
        const updated = await ctx.prisma.message.update({
          where: {
            id,
          },
          data: {
            text: text || find?.text,
          },
        });
        ctx.pubsub.publish('editMessage', {
          editMessage: updated
        })
        return updated;
      },
    });
    t.field("deleteMessage", {
      type: "Message",
      args: {
        id: nonNull(stringArg()),
      },
      resolve: async (_parent, { id }, ctx) => {
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const deleted = await ctx.prisma.message.update({
          where: {
            id,
          },
          data: {
            isDeleted: true,
          },
        });
        ctx.pubsub.publish('deleteMessage', {
          deleteMessage: deleted
        })
        return deleted;
      },
    });
  },
});
