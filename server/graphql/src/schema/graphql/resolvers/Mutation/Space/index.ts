import { uniq } from "lodash";
import { extendType, inputObjectType, nonNull } from "nexus";
import { combination } from "../../../../../utils/utils";

export const InputCreateSpace = inputObjectType({
  name: "InputCreateSpace",
  definition(t) {
    t.nonNull.string("name");
    t.list.nonNull.string("usersToEmail");
    t.string("subject");
  },
});

export const InputCreateInviteUser = inputObjectType({
  name: "InputCreateInviteUser",
  definition(t) {
    t.nonNull.string("id");
    t.list.nonNull.string("usersToEmail");
  },
});
export const SpaceMutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createSpace", {
      type: "Space",
      args: {
        inputCreateSpace: nonNull(InputCreateSpace),
      },
      resolve: async (_parent, { inputCreateSpace }, ctx) => {
        const { name, usersToEmail, subject } = inputCreateSpace;
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const createSpace = await ctx.prisma.space.create({
          data: {
            name,
            authorId: ctx.currentUser.id,
          },
        });
        // create default channel
        const defaultChannel = [
          {
            name: "général",
          },
          {
            name: "aléatoire",
          },
        ];
        if (subject?.length)
          defaultChannel.push({
            name: subject,
          });
        await Promise.all(
          defaultChannel.map((channel) =>
            ctx.prisma.channel.create({
              data: {
                isPrivate: false,
                name: channel.name,
                spaceId: createSpace.id,
              },
            })
          )
        );

        // invite user by email
        const usersToInvite = await ctx.prisma.user.findMany({
          where: {
            email: {
              in: uniq([...(usersToEmail || []), ctx.currentUser.email]),
            },
          },
        });
        await Promise.all(
          usersToInvite.map(async (user) =>
            ctx.prisma.userSpace.create({
              data: {
                spaceId: createSpace.id,
                userId: user.id,
              },
            })
          )
        );
        // Create directs channel
        const allCombinaison = combination(usersToInvite);
        await Promise.all(
          allCombinaison.map(async (i) => {
            let findExist = await ctx.prisma.channel.findFirst({
              where: {
                name: {
                  in: [
                    `${createSpace.id}:${i.a.id}:${i.b.id}`,
                    `${createSpace.id}:${i.b.id}:${i.a.id}`,
                  ],
                },
                spaceId: createSpace.id,
                isDirectChannel: true,
                isPrivate: false,
              },
            });
            if (!findExist) {
              findExist = await ctx.prisma.channel.create({
                data: {
                  isPrivate: false,
                  isDirectChannel: true,
                  spaceId: createSpace.id,
                  name: `${createSpace.id}:${i.a.id}:${i.b.id}`,
                },
              });
            }
            if (!findExist?.id) return [];
            const userChannels = await Promise.all(
              [i.a.id, i.b.id].map(async (e) => {
                const find = await ctx.prisma.userChannel.findFirst({
                  where: {
                    channelId: findExist?.id || "",
                    userId: e,
                  },
                });
                if (!find) {
                  const userChannels = await ctx.prisma.userChannel.create({
                    data: {
                      channelId: findExist?.id || "",
                      userId: e,
                    },
                  });
                  return userChannels;
                }
                return find;
              })
            );

            return userChannels;
          })
        );
        return createSpace;
      },
    });
    t.field("createInviteUser", {
      type: "Space",
      args: {
        inputCreateInviteUser: nonNull(InputCreateInviteUser),
      },
      resolve: async (_parent, { inputCreateInviteUser }, ctx) => {
        const { id, usersToEmail } = inputCreateInviteUser;
        if (!ctx.currentUser?.id || !ctx.currentUser.email)
          throw new Error("not-connected");
        const findSpace = await ctx.prisma.space.findUnique({
          where: {
            id,
          },
        });
        if(!findSpace) return null;
        // invite user by email
        const usersToInvite = await ctx.prisma.user.findMany({
          where: {
            email: {
              in: uniq([...(usersToEmail || [])]),
            },
          },
        });
        await Promise.all(
          usersToInvite.map(async (user) =>{
            const findIfExist = await ctx.prisma.userSpace.findFirst({
              where: {
                spaceId: findSpace.id,
                userId: user.id,
              }
            })
            if(findIfExist) return findIfExist;
            return ctx.prisma.userSpace.create({
              data: {
                spaceId: findSpace.id,
                userId: user.id,
              },
            })
          }
            
          )
        );
         const allUserSpaces = await ctx.prisma.userSpace.findMany({
           where: {
             spaceId: findSpace.id,
           },
           distinct: "userId"
         });
         // Create directs channel
         const allCombinaison = combination(allUserSpaces);
         await Promise.all(
           allCombinaison.map(async (i) => {
             let findExist = await ctx.prisma.channel.findFirst({
               where: {
                 name: {
                   in: [
                     `${findSpace.id}:${i.a.userId}:${i.b.userId}`,
                     `${findSpace.id}:${i.b.userId}:${i.a.userId}`,
                   ],
                 },
                 spaceId: findSpace.id,
                 isDirectChannel: true,
                 isPrivate: false,
               },
             });
             if (!findExist) {
               findExist = await ctx.prisma.channel.create({
                 data: {
                   isPrivate: false,
                   isDirectChannel: true,
                   spaceId: findSpace.id,
                   name: `${findSpace.id}:${i.a.userId}:${i.b.userId}`,
                 },
               });
             }
             if (!findExist?.id) return [];
             const userChannels = await Promise.all(
               [i.a.userId, i.b.userId].map(async (e) => {
                 const find = await ctx.prisma.userChannel.findFirst({
                   where: {
                     channelId: findExist?.id || "",
                     userId: e,
                   },
                 });
                 if (!find) {
                   const userChannels = await ctx.prisma.userChannel.create({
                     data: {
                       channelId: findExist?.id || "",
                       userId: e,
                     },
                   });
                   return userChannels;
                 }
                 return find;
               })
             );
 
             return userChannels;
           })
         );

        return findSpace;
      },
    });
  },
});
