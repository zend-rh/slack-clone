import { extendType, nonNull, stringArg } from "nexus";

export const ChannelQuery = extendType({
  type: "Query",
  definition(t) {
    t.field("channel", {
      args: {
        id: nonNull(stringArg()),
      },
      type: "Channel",
      resolve: async (_parent, args, ctx) => {
        return ctx.prisma.channel.findUnique({
          where: {
            id: args.id,
          },
        });
      },
    });
  },
});
