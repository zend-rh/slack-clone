import { extendType, nonNull, stringArg } from "nexus";

export const SpaceQuery = extendType({
  type: "Query",
  definition(t) {
    t.field("space", {
      args: {
        id: nonNull(stringArg()),
      },
      type: "Space",
      resolve: async (_parent, args, ctx) => {
        return ctx.prisma.space.findUnique({
          where: {
            id: args.id,
          },
        });
      },
    });
  },
});
