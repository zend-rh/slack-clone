import { extendType } from "nexus";

export const QueryUser = extendType({
  type: "Query",
  definition(t) {
    t.field("me", {
      type: "User",
      resolve: async (_parent, _args, ctx) => {
        return ctx.prisma.user.findUnique({
          where: {
            id: ctx.currentUser?.id,
          },
        });
      },
    });
  },
});
