import { extendType, nonNull, stringArg } from "nexus";
import { resolveSubscribe, subscribeWithFilter } from "../utils";

export const SubscriptionChannel = extendType({
  type: "Subscription",
  definition(t) {
    t.field("newChannel", {
      type: "Channel",
      args: {
        spaceId: nonNull(stringArg()),
      },
      subscribe: subscribeWithFilter(
        "newChannel",
        async (payload, args, ctx) => {
          if (!payload.newChannel || !args.spaceId || !ctx?.currentUser)
            return false;
          if (
            payload.newChannel.spaceId === args.spaceId &&
            !payload.newChannel.isDirectChannel
          ) {
            if (payload.newChannel.isPrivate) {
              const b = await ctx.prisma.userChannel.findFirst({
                where: {
                  userId: ctx.currentUser.id,
                  channelId: payload.newChannel.id,
                },
              });
              return Boolean(b)
            }
            return true;
          }
          return false;
        }
      ),
      resolve: resolveSubscribe("newChannel", (payload) => {
        return payload.newChannel;
      }),
    });
    t.field("deleteChannel", {
      type: "Channel",
      args: {
        spaceId: nonNull(stringArg()),
      },
      subscribe: subscribeWithFilter(
        "deleteChannel",
        async (payload, args, ctx) => {
          if (!payload.deleteChannel || !args.spaceId) return false;
          return payload.deleteChannel.spaceId === args.spaceId;
        }
      ),
      resolve: resolveSubscribe("deleteChannel", (payload) => {
        return payload.deleteChannel;
      }),
    });
    t.field("editChannel", {
      type: "Channel",
      args: {
        spaceId: nonNull(stringArg()),
      },
      subscribe: subscribeWithFilter(
        "editChannel",
        async (payload, args, ctx) => {
          if (!payload.editChannel || !args.spaceId) return false;
          return payload.editChannel.spaceId === args.spaceId;
        }
      ),
      resolve: resolveSubscribe("editChannel", (payload) => {
        return payload.editChannel;
      }),
    });
  },
});
