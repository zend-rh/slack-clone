import { extendType, nonNull, stringArg } from "nexus";
import { resolveSubscribe, subscribeWithFilter } from "../utils";

export const SubscriptionMessage = extendType({
  type: "Subscription",
  definition(t) {
    t.field("newMessage", {
      type: "Message",
      args: {
        channelId: nonNull(stringArg()),
      },
      subscribe: subscribeWithFilter(
        "newMessage",
        async (payload, args, ctx) => {
          if (!payload.newMessage || !args.channelId) return false;
          return payload.newMessage.channelId === args.channelId;
        }
      ),
      resolve: resolveSubscribe("newMessage", (payload) => {
        return payload.newMessage;
      }),
    });
    t.field("deleteMessage", {
      type: "Message",
      args: {
        channelId: nonNull(stringArg()),
      },
      subscribe: subscribeWithFilter(
        "deleteMessage",
        async (payload, args, ctx) => {
          if (!payload.deleteMessage || !args.channelId) return false;
          return payload.deleteMessage.channelId === args.channelId;
        }
      ),
      resolve: resolveSubscribe("deleteMessage", (payload) => {
        return payload.deleteMessage;
      }),
    });
    t.field("editMessage", {
      type: "Message",
      args: {
        channelId: nonNull(stringArg()),
      },
      subscribe: subscribeWithFilter(
        "editMessage",
        async (payload, args, ctx) => {
          if (!payload.editMessage || !args.channelId) return false;
          return payload.editMessage.channelId === args.channelId;
        }
      ),
      resolve: resolveSubscribe("editMessage", (payload) => {
        return payload.editMessage;
      }),
    });
  },
});
