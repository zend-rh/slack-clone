import { withFilter } from "graphql-subscriptions";
import { extendType, nonNull, stringArg } from "nexus";
import { resolveSubscribe, subscribeWithFilter } from "../utils";

export const SubscriptionUser = extendType({
  type: "Subscription",
  definition(t) {
    t.field("editUser", {
      type: "User",
      args: {
        userId: nonNull(stringArg()),
      },
      subscribe: subscribeWithFilter("editUser", async (payload, args, ctx) => {
        if (!payload.editUser || !args.userId) return false;
        return payload.editUser.id === args.userId;
      }),
      resolve: resolveSubscribe("editUser", (payload) => {
        return payload.editUser;
      }),
    });
    t.field("updateUser", {
      type: "User",
      subscribe: subscribeWithFilter(
        "updateUser",
        async (payload, args, ctx) => {
          if (ctx.currentUser) return true;
          return false;
        }
      ),
      resolve: resolveSubscribe("updateUser", (payload) => {
        return payload.updateUser;
      }),
    });
  },
});
