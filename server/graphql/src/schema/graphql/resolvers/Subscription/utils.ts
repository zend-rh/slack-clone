import { withFilter } from "graphql-subscriptions";
import { Context } from "../../../../types/contextType";
import { NexusGenArgTypes, NexusGenFieldTypes } from "../../../generated/nexus";

export const subscribeWithFilter = <
  T extends keyof NexusGenFieldTypes["Subscription"],
  Args = any,
  PayloadType = Record<string, never>
>(
  subscriptionName: T,
  filterFunction: (
    payload: PayloadType & Record<T, NexusGenFieldTypes["Subscription"][T]>,
    args: NexusGenArgTypes["Subscription"][T],
    context: Context
  ) => Promise<boolean>
) =>
  withFilter(
    (_, __, ctx: Context) => ctx.pubsub.asyncIterator(subscriptionName),
    filterFunction
  );

export const resolveSubscribe = <
  T extends keyof NexusGenFieldTypes["Subscription"],
  Args = any,
  PayloadType = Record<string, never>
>(
  subscriptionName: T,
  resolveFunction: (
    payload: PayloadType & Record<T, NexusGenFieldTypes["Subscription"][T]>,
    args: NexusGenArgTypes["Subscription"][T],
    context: Context
  ) => PayloadType
) => resolveFunction;
