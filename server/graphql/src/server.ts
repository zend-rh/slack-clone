/* eslint-disable no-new */
import { ApolloServer } from "apollo-server-express";
import { execute, subscribe } from "graphql";
import express from "express";
import { createServer } from "http";
import { createContext, createContextWS, setStatus } from "./configs/context";
import { configureRedis } from "./configs/redis";
import { schema } from "./schema/schema";
import { SubscriptionServer } from "subscriptions-transport-ws";
import { Context } from "./types/contextType";

const port = process.env.NODE_PORT || 4000;

const { redis, pubsub } = configureRedis();

const app = express();

const httpServer = createServer(app);

const subscriptionServer = SubscriptionServer.create(
  {
    schema: schema as any,
    execute,
    subscribe,
    onConnect: async (connectionParams: { authToken?: string }) => {
      return createContextWS(connectionParams, redis, pubsub, "online");
    },
    onDisconnect: async (_ : any, context: any) => {
      const c: Context = await context.initPromise;
      await setStatus(c.currentUser, redis, pubsub, "offline");
      return c;
    },
  },
  {
    server: httpServer,
    path: "/graphql",
  }
);
const server = new ApolloServer({
  schema: schema as any,
  context: (req) => createContext(req, redis, pubsub),
  plugins: [
    {
      async serverWillStart() {
        return {
          async drainServer() {
            subscriptionServer.close();
          },
        };
      },
    },
  ],
});

server.start().then(() => {
  server.applyMiddleware({ app });
  httpServer.listen(port, () => {
    console.log(
      `🚀 Server is now running on http://localhost:${port}${server.graphqlPath}`
    );
    console.log(
      `🚀 Surbscription ready at: ws://localhost:${port}${server.graphqlPath}\n⭐️ `
    );
  });
});
