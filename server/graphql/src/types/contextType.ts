/* eslint-disable import/no-extraneous-dependencies */

import { PrismaClient, User } from "@prisma/client";
import { PubSub } from "graphql-subscriptions";
import { Redis } from "ioredis";
import { PubSubType } from "../configs/redis";

export type Context = {
  prisma: PrismaClient;
  pubsub: PubSubType;
  currentUser: User | null;
  userId: string;
  redis: Redis;
};
