export const getRandomDigit = (lenght = 6) => {
  return Math.random().toFixed(lenght).split(".")[1];
};

export const combination = <T>(
  tab: T[]
): {
  a: T;
  b: T;
}[] => {
  let results: {
    a: T;
    b: T;
  }[] = [];
  for (let i = 0; i < tab.length - 1; i++) {
    for (let j = i + 1; j < tab.length; j++) {
      results.push({
        a: tab[i],
        b: tab[j],
      });
    }
  }
  return results;
};
